Requires Java 1.8_45

In addition to being a replay viewer, this project is also designed to test agents which implement the testable interface I wrote. There are many model tests. The gui can also simulate games if an agent with a working model is added as a library (implementing the testable interfaces). My agent is currently added as a library, so games with my various agents can be simulated. The tester itself doesn't have a model, only basic stuff to animate.

I wrote all the tests, but there may be other entries which also implement the interfaces for testing purposes. Since the interfaces contain no logic I assume this is fine. The idea was that we share tests, particular tests relating to the model, but not the model itself. Since entrants could use the test harness code (your model) and the templates you provided, I don't expect shared tests to be a violation. 
