package personaltests;

import java.io.File;
import java.io.IOException;
import utilities.MoveSimulator;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class ManualTests {

    public static void main(String args[]) throws IOException {
String state ="###################\n" +
"# Alien Expectimax #\n" +
"# Wave Size:  5   #\n" +
"# Delta X:  1     #\n" +
"# Energy:  0/6    #\n" +
"# Respawn: -1     #\n" +
"# Round: 120      #\n" +
"# Kills:  20      #\n" +
"# Lives: 997        #\n" +
"# Missiles: 1/2   #\n" +
"###################\n" +
"#       XXXMMM    #\n" +
"#       VVV       #\n" +
"# ---         --- #\n" +
"#  -          --- #\n" +
"#  -           -- #\n" +
"#                 #\n" +
"#         i       #\n" +
"#                 #\n" +
"#  |              #\n" +
"#  x  x           #\n" +
"#                 #\n" +
"#                 #\n" +
"#                 #\n" +
"#x  x  x  x       #\n" +
"#                 #\n" +
"#x  x  x  x       #\n" +
"#                 #\n" +
"#   x  x          #\n" +
"#  -   |      !   #\n" +
"# ---             #\n" +
"# ---             #\n" +
"#             AAA #\n" +
"#          | MMM  #\n" +
"###################\n" +
"# Missiles: 1/2   #\n" +
"# Lives: 1        #\n" +
"# Kills:  27      #\n" +
"# Round: 120      #\n" +
"# Respawn: -1     #\n" +
"# Energy:  0/6    #\n" +
"# Delta X: -1     #\n" +
"# Wave Size:  4   #\n" +
"# Right Expectimax #\n" +
"###################";
        
        File outfolder = new File ("./replays/tests/0010");
        MoveSimulator.simulate(state, outfolder, new Move[]{Move.FIRE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE}, new Move[]{Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE, Move.NONE});
    }
}
