/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeltests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utilities.AgentInitialiser;
import utilities.MoveSimulator;
import utilities.StateComparisonException;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;
import za.ac.wits.witstestablespaceinvaders.TestableGameState;

/**
 *
 * @author Dean
 */
public class AlienSpawnTest {

    public AlienSpawnTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void alienSpawnTest1() throws IOException, StateComparisonException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 4/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  22      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/2   #\n"
                + "###################\n"
                + "#       MMM       #\n"
                + "#      VVV        #\n"
                + "# ---     -   --- #\n"
                + "# ---   ---   --- #\n"
                + "#  --   ---|  --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#           x  x  #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          x  x  x#\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---          -- #\n"
                + "# ---         --- #\n"
                + "# ---    !    --- #\n"
                + "#       AAA       #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 0        #\n"
                + "# Kills: 0        #\n"
                + "# Round:  22      #\n"
                + "# Respawn: 0      #\n"
                + "# Energy: 4/6     #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################\n";

        String expectedResult = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  23      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/2   #\n"
                + "###################\n"
                + "#       MMM       #\n"
                + "#      VVV        #\n"
                + "# ---     -   --- #\n"
                + "# ---   ---|  --- #\n"
                + "#  --   ---   --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#            x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#         x  x  x #\n"
                + "#                 #\n"
                + "#         x  x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---          -- #\n"
                + "# ---    !    --- #\n"
                + "# ---         --- #\n"
                + "#       AAA       #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 0        #\n"
                + "# Kills: 0        #\n"
                + "# Round:  23      #\n"
                + "# Respawn:  0      #\n"
                + "# Energy: 5/6     #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################\n";
        
        assertTrue("Missiles should die. Alien escapes.", MoveSimulator.expectedResult(state, Move.NONE, Move.NONE, expectedResult));
    }
    
        @Test
    public void alienSpawnTest2() throws IOException, StateComparisonException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 4/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  22      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/2   #\n"
                + "###################\n"
                + "#       MMM       #\n"
                + "#      VVV        #\n"
                + "# ---     -   --- #\n"
                + "# ---   ---   --- #\n"
                + "#  --   ---|  --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#           x  x  #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          x  x   #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---          -- #\n"
                + "# ---         --- #\n"
                + "# ---    !    --- #\n"
                + "#       AAA       #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 0        #\n"
                + "# Kills: 0        #\n"
                + "# Round:  22      #\n"
                + "# Respawn: 0      #\n"
                + "# Energy: 4/6     #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################\n";

        String expectedResult = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  23      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/2   #\n"
                + "###################\n"
                + "#       MMM       #\n"
                + "#      VVV        #\n"
                + "# ---     -   --- #\n"
                + "# ---   ---|  --- #\n"
                + "#  --   ---   --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#            x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#      x  x  x    #\n"
                + "#                 #\n"
                + "#         x  x    #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---          -- #\n"
                + "# ---    !    --- #\n"
                + "# ---         --- #\n"
                + "#       AAA       #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 0        #\n"
                + "# Kills: 0        #\n"
                + "# Round:  23      #\n"
                + "# Respawn:  0      #\n"
                + "# Energy: 5/6     #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################\n";
        
        assertTrue("Aliens did not spawn as expected.", MoveSimulator.expectedResult(state, Move.NONE, Move.NONE, expectedResult));
    }
}
