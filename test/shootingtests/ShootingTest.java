/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shootingtests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javafx.geometry.Point2D;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utilities.AgentInitialiser;
import utilities.MoveSimulator;
import utilities.StateComparisonException;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;
import za.ac.wits.witstestablespaceinvaders.TestableGameState;

/**
 *
 * @author Dean
 */
public class ShootingTest {

    public ShootingTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getBullets1() throws IOException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   1      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#         x  x  x #\n"
                + "#                 #\n"
                + "#         x  x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#        AAA      #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 0/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills:   0      #\n"
                + "# Round:   1      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################";
        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        assertTrue("Aliens should not be able to fire bullets in next round.", testable.getPlayer1AlienPrimaryBullet() == null);
        assertTrue("Aliens should not be able to fire bullets in next round.", testable.getPlayer2AlienPrimaryBullet() == null);
        assertTrue("Aliens should not be able to fire bullets in next round.", testable.getPlayer1AlienSecondaryBullets().isEmpty());
        assertTrue("Aliens should not be able to fire bullets in next round.", testable.getPlayer2AlienSecondaryBullets().isEmpty());
    }

    @Test
    public void getBullets2() throws IOException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   1      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#         x  x  x #\n"
                + "#                 #\n"
                + "#         x  x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#        AAA      #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 0/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills:   0      #\n"
                + "# Round:   1      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################";
        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        List<Point2D> secondaryBullets = testable.getPlayer1AlienSecondaryBullets();
        assertTrue("Middle alien on player 1's side could fire.", testable.getPlayer1AlienPrimaryBullet().equals(new Point2D(8, 13)));
        assertTrue("Should be 2 secondary shooters.", secondaryBullets.size() == 2);
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(11, 13)));
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(14, 13)));

        assertTrue("Aliens should not be able to fire bullets in next round.", testable.getPlayer2AlienPrimaryBullet() == null);
        assertTrue("Aliens should not be able to fire bullets in next round.", testable.getPlayer2AlienSecondaryBullets().isEmpty());
    }
    
        @Test
    public void getBullets3() throws IOException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   1      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#         x  x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#        AAA      #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 0/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills:   0      #\n"
                + "# Round:   1      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################";
        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        List<Point2D> secondaryBullets = testable.getPlayer1AlienSecondaryBullets();
        assertTrue("Middle alien on player 1's side could fire.", testable.getPlayer1AlienPrimaryBullet().equals(new Point2D(9, 13)));
        assertTrue("Should be 2 secondary shooters.", secondaryBullets.size() == 2);
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(12, 13)));
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(15, 13)));

        assertTrue("Aliens should not be able to fire bullets in next round.", testable.getPlayer2AlienPrimaryBullet() == null);
        assertTrue("Aliens should not be able to fire bullets in next round.", testable.getPlayer2AlienSecondaryBullets().isEmpty());
    }
    
            @Test
    public void getBullets4() throws IOException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   1      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#        AAA      #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 0/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills:   0      #\n"
                + "# Round:   1      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################";
        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        List<Point2D> secondaryBullets = testable.getPlayer1AlienSecondaryBullets();
        assertTrue("Middle alien on player 1's side could fire.", testable.getPlayer1AlienPrimaryBullet().equals(new Point2D(9, 13)));
        assertTrue("Should be 2 secondary shooters.", secondaryBullets.size() == 2);
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(12, 13)));
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(15, 13)));

        secondaryBullets = testable.getPlayer2AlienSecondaryBullets();
        assertTrue("Middle alien on player 1's side could fire.", testable.getPlayer2AlienPrimaryBullet().equals(new Point2D(9, 9)));
        assertTrue("Should be 2 secondary shooters.", secondaryBullets.size() == 2);
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(12, 9)));
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(15, 9)));
    }
    
                @Test
    public void testWaveSizeChange() throws IOException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  39      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#        AAA      #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 0/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills:   0      #\n"
                + "# Round:  39      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################";
        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        List<Point2D> secondaryBullets = testable.getPlayer1AlienSecondaryBullets();
        assertTrue("Middle alien on player 1's side could fire.", testable.getPlayer1AlienPrimaryBullet().equals(new Point2D(9, 13)));
        assertTrue("Should be 2 secondary shooters.", secondaryBullets.size() == 3);
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(12, 13)));
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(15, 13)));
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(6, 13)));
        
        secondaryBullets = testable.getPlayer2AlienSecondaryBullets();
        assertTrue("Middle alien on player 1's side could fire.", testable.getPlayer2AlienPrimaryBullet().equals(new Point2D(9, 9)));
        assertTrue("Should be 2 secondary shooters.", secondaryBullets.size() == 3);
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(12, 9)));
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(15, 9)));
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(6, 9)));
    }
    
    
    @Test
    public void testMultipleWaves() throws IOException {
                String state = ""
                + "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Respawn: 0      #\n"
                + "# Round:  53      #\n"
                + "# Kills: 2        #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 1/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#         VVV     #\n"
                + "# ---  |      --- #\n"
                + "# ---         --- #\n"
                + "#   -         --- #\n"
                + "#          i      #\n"
                + "#    x  x         #\n"
                + "#                 #\n"
                + "# x  x  x         #\n"
                + "#                 #\n"
                + "# x  x  x         #\n"
                + "#                 #\n"
                + "#    x  x  x  x   #\n"
                + "#                 #\n"
                + "#          x      #\n"
                + "#                 #\n"
                + "#             x   #\n"
                + "#                 #\n"
                + "#  --      !  --  #\n"
                + "# ---         --  #\n"
                + "# ---         --- #\n"
                + "#       AAA       #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills: 2        #\n"
                + "# Round:  53      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  4   #\n"
                + "# Player 1        #\n"
                + "###################\n";
        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        List<Point2D> secondaryBullets = testable.getPlayer1AlienSecondaryBullets();
        assertTrue("Middle alien on player 1's side could fire.", testable.getPlayer1AlienPrimaryBullet().equals(new Point2D(12, 17)));
        assertTrue("Should be 1 secondary shooters.", secondaryBullets.size() == 1);
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(9, 15)));

        
        secondaryBullets = testable.getPlayer2AlienSecondaryBullets();
        assertTrue("Middle alien on player 1's side could fire.", testable.getPlayer2AlienPrimaryBullet().equals(new Point2D(6, 5)));
        assertTrue("Should be 2 secondary shooters.", secondaryBullets.size() == 2);
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(3, 5)));
        assertTrue("Should contain shooter.", secondaryBullets.contains(new Point2D(0, 7)));
    }
        @Test
    public void testMultipleWavesShoot() throws IOException, StateComparisonException {
                String state = ""
                + "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Respawn: 0      #\n"
                + "# Round:  53      #\n"
                + "# Kills: 2        #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 1/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#         VVV     #\n"
                + "# ---  |      --- #\n"
                + "# ---         --- #\n"
                + "#   -         --- #\n"
                + "#          i      #\n"
                + "#    x  x         #\n"
                + "#                 #\n"
                + "# x  x  x         #\n"
                + "#                 #\n"
                + "# x  x  x         #\n"
                + "#                 #\n"
                + "#    x  x  x  x   #\n"
                + "#                 #\n"
                + "#          x      #\n"
                + "#                 #\n"
                + "#             x   #\n"
                + "#                 #\n"
                + "#  --      !  --  #\n"
                + "# ---         --  #\n"
                + "# ---         --- #\n"
                + "#       AAA       #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills: 2        #\n"
                + "# Round:  53      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  4   #\n"
                + "# Player 1        #\n"
                + "###################\n";
                
                                String result = ""
                + "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 0/6     #\n"
                + "# Respawn: 0      #\n"
                + "# Round:  54      #\n"
                + "# Kills: 2        #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 1/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#      |  VVV     #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#   -         --- #\n"
                + "#      |          #\n"
                + "#   x  x   i      #\n"
                + "#                 #\n"
                + "#x  x  x          #\n"
                + "#                 #\n"
                + "#x  x  x          #\n"
                + "#                 #\n"
                + "#   x  x  x  x    #\n"
                + "#                 #\n"
                + "#         x       #\n"
                + "#                 #\n"
                + "#            x    #\n"
                + "#          ! |    #\n"
                + "#  --         --  #\n"
                + "# ---         --  #\n"
                + "# ---         --- #\n"
                + "#       AAA       #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills: 2        #\n"
                + "# Round:  54      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 0/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  4   #\n"
                + "# Player 1        #\n"
                + "###################\n";
                LinkedList<Point2D> bullets = new LinkedList<Point2D>();
                bullets.add(new Point2D(12, 17));
                bullets.add(new Point2D(6, 5));
                        assertTrue("Missiles should die. Alien escapes.", MoveSimulator.expectedResult(state, Move.NONE, Move.NONE, result, bullets));
    }
    
            @Test
    public void testBulletOnMissile() throws IOException, StateComparisonException {
                String state = "###################\n" +
"# Alien MiniMax   #\n" +
"# Wave Size:  5   #\n" +
"# Delta X:  1     #\n" +
"# Energy:  5/6    #\n" +
"# Respawn: -1     #\n" +
"# Round: 107      #\n" +
"# Kills:  12      #\n" +
"# Lives: 0        #\n" +
"# Missiles: 1/1   #\n" +
"###################\n" +
"#       XXX       #\n" +
"#       VVV       #\n" +
"# ---         --- #\n" +
"# - -        |--- #\n" +
"#   -         - - #\n" +
"#                 #\n" +
"#                 #\n" +
"#                 #\n" +
"#       i         #\n" +
"#       x  x      #\n" +
"#                 #\n" +
"#                 #\n" +
"#                 #\n" +
"#x  x  x  x  x    #\n" +
"#                 #\n" +
"#   x  x  x  x !  #\n" +
"#                 #\n" +
"#      x      !   #\n" +
"#  -              #\n" +
"# ---             #\n" +
"# ---   |         #\n" +
"#           AAA   #\n" +
"#            MMM  #\n" +
"###################\n" +
"# Missiles: 2/2   #\n" +
"# Lives: 1        #\n" +
"# Kills:  22      #\n" +
"# Round: 107      #\n" +
"# Respawn: -1     #\n" +
"# Energy:  5/6    #\n" +
"# Delta X: -1     #\n" +
"# Wave Size:  4   #\n" +
"# Right MiniMax   #\n" +
"###################";
                
                                String result = "###################\n" +
"# Alien MiniMax   #\n" +
"# Wave Size:  5   #\n" +
"# Delta X:  1     #\n" +
"# Energy:  0/6    #\n" +
"# Respawn: -1     #\n" +
"# Round: 108      #\n" +
"# Kills:  13      #\n" +
"# Lives: 0        #\n" +
"# Missiles: 0/1   #\n" +
"###################\n" +
"#       XXX       #\n" +
"#        VVV      #\n" +
"# ---        |--- #\n" +
"# - -         --- #\n" +
"#   -         - - #\n" +
"#                 #\n" +
"#                 #\n" +
"#                 #\n" +
"#         |       #\n" +
"#         x       #\n" +
"#                 #\n" +
"#                 #\n" +
"#                 #\n" +
"# x  x  x  x  x   #\n" +
"#              !  #\n" +
"#    x  x  x  x   #\n" +
"#                 #\n" +
"#       x         #\n" +
"#  -              #\n" +
"# ---             #\n" +
"# ---             #\n" +
"#       |  AAA    #\n" +
"#            MMM  #\n" +
"###################\n" +
"# Missiles: 1/2   #\n" +
"# Lives: 1        #\n" +
"# Kills:  22      #\n" +
"# Round: 108      #\n" +
"# Respawn: -1     #\n" +
"# Energy:  0/6    #\n" +
"# Delta X: -1     #\n" +
"# Wave Size:  4   #\n" +
"# Right MiniMax   #\n" +
"###################";
                LinkedList<Point2D> bullets = new LinkedList<Point2D>();
                bullets.add(new Point2D(13, 16));
                bullets.add(new Point2D(9, 8));
                        assertTrue("Bullet and missile should collide.", MoveSimulator.expectedResult(state, Move.LEFT, Move.LEFT, result, bullets));
    }
    
    
    //test 2 waves
    //test 3 waves
    //test 2 waves separated by 3 spaces
    //test 1 loner on edge
    //test fire as respawn second wave move forward
    //test aliens in 1 column
    //test 1 alien
    
    //test shoot
    //test alien dies before shooting
}
