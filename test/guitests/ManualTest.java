package guitests;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.junit.Ignore;
import spaceinvaderstester.BoardPane;
import spaceinvaderstester.GameState;
import spaceinvaderstester.GameStateBrowser;
import spaceinvaderstester.boardelements.Alien;
import spaceinvaderstester.boardelements.Bullet;
import spaceinvaderstester.boardelements.Missile;
@Ignore
public class ManualTest extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Internal Window Test");
        StackPane root = new StackPane();
//        BoardPane board = new BoardPane();
        GameStateBrowser browser = new GameStateBrowser();
        GameState zero = new GameState("test/testfiles/mapadvanced0.txt");
        GameState one = new GameState("test/testfiles/mapadvanced1.txt");
        GameState two = new GameState("test/testfiles/mapadvanced2.txt");
        GameState three = new GameState("test/testfiles/mapadvanced3.txt");

        root.getChildren().add(browser);

        primaryStage.setScene(new Scene(root, 500, 625));
        primaryStage.show();
        browser.addState(zero);
        browser.addState(one);
		browser.addState(two);
		browser.addState(three);
        /*
        board.setOnMouseClicked((e) -> {
            board.transitionState(one, 500);
            board.setOnMouseClicked((e2) -> {
                board.transitionState(two, 500);
                board.setOnMouseClicked((e3) -> {
                    board.transitionState(three, 500);
                });
            });
        });
                */
        //Thread.sleep(5000);

        //Thread.sleep(500);
        //board.transitionState(three, 500);
    }

    public static void main(String args[]) {
        launch(args);
    }

}
