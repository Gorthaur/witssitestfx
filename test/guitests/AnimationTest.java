package guitests;

import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.junit.Ignore;
import spaceinvaderstester.GameState;
import spaceinvaderstester.GameStateBrowser;

/**
 *
 * @author Dean
 */
@Ignore
public class AnimationTest extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Internal Window Test");
        Pane root = new Pane();

        primaryStage.setScene(new Scene(root, 500, 625));
        primaryStage.show();
        Rectangle red = new Rectangle(0, 0, 10, 10);
        red.setFill(Color.RED);
        Rectangle blue = new Rectangle(0, 0, 10, 10);
        blue.setFill(Color.BLUE);
        TranslateTransition t = new TranslateTransition(Duration.millis(10000), red);
        t.setFromX(0);
        t.setFromY(0);
        t.setToX(490);
        t.setToY(615);

        TranslateTransition t2 = new TranslateTransition(Duration.millis(5000), blue);
        t2.setFromX(0);
        t2.setFromY(0);
        t2.setToX(490);
        t2.setToY(615);

        t2.setOnFinished((e) -> {
            System.out.println("woah");
            blue.setFill(Color.GREEN);
        });

        ParallelTransition p1 = new ParallelTransition(t);
        ParallelTransition p2 = new ParallelTransition(t2);
        //Platform.runLater(() -> {
        root.getChildren().addAll(red, blue);
        p1.playFromStart();
        p2.playFromStart();
        Thread.sleep(2000);
        p2.pause();
        p2.jumpTo(Duration.millis(100000));
        p2.play();
        t2.jumpTo(Duration.millis(100000));
        //p2.
        //});

    }

    public static void main(String args[]) {
        launch(args);
    }

}
