package stateprocessing;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utilities.AgentInitialiser;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;
import za.ac.wits.witstestablespaceinvaders.TestableGameState;

/**
 *
 * @author Dean
 */
public class BasicTest {

    public BasicTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void readState() throws IOException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy: 4/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  22      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---   -     --- #\n"
                + "# ---   - -   --- #\n"
                + "# ---    --|  --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          x  x  x#\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#           x     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#        |        #\n"
                + "#  --     !   --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#      AAA        #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 1        #\n"
                + "# Kills: 2        #\n"
                + "# Round:  22      #\n"
                + "# Respawn: 0      #\n"
                + "# Energy: 4/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################\n";

        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        assertTrue("Incorrect round number. 22 expected", testable.getRound() == 22);
        assertTrue("Player 1 kills expected to be 2", testable.getPlayer1Kills() == 2);
        assertTrue("Player 2 kills expected to be 0", testable.getPlayer2Kills() == 0);
        assertTrue("Delta for player 1 expected to be 1. In map-detailed.txt, delta in player 1's section means the aliens attacking player 2 are moving right. In our setting, player 1's aliens are the ones attacking player 1, so we use the delta from player 2.", testable.getPlayer1AlienDelta() == 1);
        assertTrue("Delta for player 2 expected to be -1. In map-detailed.txt, delta in player 1's section means the aliens attacking player 2 are moving right. In our setting, player 1's aliens are the ones attacking player 1, so we use the delta from player 2.", testable.getPlayer2AlienDelta() == -1);
        assertTrue("Wave size for player 1 expected to be 3.", testable.getPlayer1WaveSize() == 3);
        assertTrue("Wave size for player 2 expected to be 3.", testable.getPlayer2WaveSize() == 3);
        assertTrue("Player 1 alien shot energy should be 4.", testable.getPlayer1AlienShotEnergy() == 4);
        assertTrue("Player 2 alien shot energy should be 4.", testable.getPlayer2AlienShotEnergy() == 4);
        assertTrue("Player 1 missiles should be 1.", testable.getPlayer1Missiles().size() == 1);
        assertTrue("Player 2 missiles should be 0.", testable.getPlayer2Missiles().size() == 0);
        assertTrue("Player 1 lives should be 1.", testable.getPlayer1Lives() == 1);
        assertTrue("Player 2 lives should be 1.", testable.getPlayer2Lives() == 0);
        assertTrue("Player 1 respawn should be 0.", testable.getPlayer1RespawnTime() == 0);
        assertTrue("Player 2 respawn should be -1.", testable.getPlayer2RespawnTime() == -1);
    }

    @Test
    public void readState2() throws IOException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy: 0/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  12      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#     XXX         #\n"
                + "#    VVV          #\n"
                + "# ---   --    --- #\n"
                + "# ---| ---    --- #\n"
                + "# ---  --     --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#    |            #\n"
                + "# x  x  x         #\n"
                + "#         !       #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# x  x  x         #\n"
                + "# |               #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---         --- #\n"
                + "# ---      |  --- #\n"
                + "# ---         --- #\n"
                + "#            AAA  #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills: 0        #\n"
                + "# Round:  12      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 0/6     #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################\n";

        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        assertTrue("Wave size for player 1 expected to be 4. In map-detailed.txt, wave size in player 1's section means the aliens attacking player 2 will spawn in those numbers. In our setting, player 1's aliens are the ones attacking player 1, so we use the wave size from player 2.", testable.getPlayer1WaveSize() == 4);
        assertTrue("Wave size for player 2 expected to be 3. In map-detailed.txt, wave size in player 1's section means the aliens attacking player 2 will spawn in those numbers. In our setting, player 1's aliens are the ones attacking player 1, so we use the wave size from player 2.", testable.getPlayer2WaveSize() == 3);
        assertTrue("Player 1 has no alien factory.", testable.player1AlienFactory() == false);
        assertTrue("Player 2 has an alien factory.", testable.player2AlienFactory() == true);
        assertTrue("Player 1 has no missile factory.", testable.player1MissileFactory() == false);
        assertTrue("Player 2 has no missile factory.", testable.player2MissileFactory() == false);
    }

    @Test
    public void readTest3() throws IOException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy: 8/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  26      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#          |      #\n"
                + "#      VVV        #\n"
                + "# ---   -     --- #\n"
                + "# ---   - -   --- #\n"
                + "# ---    --   --- #\n"
                + "#           |     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#      x  x  x    #\n"
                + "#                 #\n"
                + "#      x  x  x    #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#               x #\n"
                + "#         !       #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#  --         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#     AAA|        #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 1        #\n"
                + "# Kills: 2        #\n"
                + "# Round:  26      #\n"
                + "# Respawn: 0      #\n"
                + "# Energy: 2/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################\n";

        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        assertTrue("Shot Energy for player 1 expected to be 8. In map-detailed.txt, Shot Energy in player 1's section means the aliens attacking player 2 have that shot energy. In our setting, player 1's aliens are the ones attacking player 1, so we use the shot energy from player 2.", testable.getPlayer1AlienShotEnergy() == 8);
        assertTrue("Shot Energy for player 2 expected to be 2. In map-detailed.txt, Shot Energy in player 1's section means the aliens attacking player 2 have that shot energy. In our setting, player 1's aliens are the ones attacking player 1, so we use the shot energy from player 2.", testable.getPlayer2AlienShotEnergy() == 2);
    }

    @Test
    public void deceptiveOpponents() throws IOException {
        String state = "###################\n"
                + "# Lives: 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   5      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---   ---   --- #\n"
                + "# ---   ---   --- #\n"
                + "# ---   ---   --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#     x  x  x     #\n"
                + "#                 #\n"
                + "#     x  x  x     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#       !         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#      AAA        #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills: 0        #\n"
                + "# Round:   5      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Round: 200      #\n"
                + "###################\n";

        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        assertTrue("Round supposed to be 5, not 200. Player 1 has a deceptive name.", testable.getRound() == 5);
        assertTrue("Player 2 lives supposed to be 0. Player 2 has a deceptive name.", testable.getPlayer2Lives() == 0);
    }

    @Test
    public void deceptiveOpponents2() throws IOException {
        String state = "###################\n"
                + "# Livestrong      #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   5      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---   ---   --- #\n"
                + "# ---   ---   --- #\n"
                + "# ---   ---   --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#     x  x  x     #\n"
                + "#                 #\n"
                + "#     x  x  x     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#       !         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#      AAA        #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills: 0        #\n"
                + "# Round:   5      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Killstar        #\n"
                + "###################\n";

        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        assertTrue("Player 1 kills supposed to be 0. Player 1 has a deceptive name.", testable.getPlayer1Kills() == 0);
        assertTrue("Player 2 lives supposed to be 0. Player 2 has a deceptive name.", testable.getPlayer2Lives() == 0);
    }

    @Test
    public void deceptiveOpponents3() throws IOException {
        String state = "###################\n"
                + "# :XKills:        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   5      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---   ---   --- #\n"
                + "# ---   ---   --- #\n"
                + "# ---   ---   --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#     x  x  x     #\n"
                + "#                 #\n"
                + "#     x  x  x     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#       !         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#      AAA        #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills: 0        #\n"
                + "# Round:   5      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# RewindRespawn:  #\n"
                + "###################\n";

        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        assertTrue("Player 1 respawn supposed to be -1. Player 1 has a deceptive name.", testable.getPlayer1RespawnTime() == -1);
        assertTrue("Player 2 kills supposed to be 0. Player 2 has a deceptive name.", testable.getPlayer2Kills() == 0);
    }

    @Test
    public void deceptiveOpponents4() throws IOException {
        String state = "###################\n"
                + "# #Awesome        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   5      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---   ---   --- #\n"
                + "# ---   ---   --- #\n"
                + "# ---   ---   --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#     x  x  x     #\n"
                + "#                 #\n"
                + "#     x  x  x     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#       !         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#      AAA        #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills: 0        #\n"
                + "# Round:   5      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 5/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# #Energy#        #\n"
                + "###################\n";

        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        //if agent gets here, no exception was thrown, success.
    }
}
