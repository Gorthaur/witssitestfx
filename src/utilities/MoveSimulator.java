package utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javafx.geometry.Point2D;
import utilities.AgentInitialiser;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;
import za.ac.wits.witstestablespaceinvaders.TestableGameState;

/**
 *
 * @author Dean
 */
public class MoveSimulator {

    public static Point2D[] flipBullets(Point2D bullets[]) {
        Point2D flipped[] = new Point2D[bullets.length];
        for (int i = 0; i < bullets.length; i++) {
            flipped[i] = new Point2D(16 - bullets[i].getX(), 22 - bullets[i].getY());
        }
        return flipped;
    }

    public static String flipBoard(String state) {
        StringBuilder builder = new StringBuilder();
        String split[] = state.trim().split("\n");
        for (int i = 44; i >= 34; i--) {
            if (i == 41) {
                String split2[] = split[i].trim().split("[\\s:]+");
                int delta = Integer.parseInt(split2[3]);
                delta = delta * -1;
                builder.append(String.format("# Delta X: %2d     #", delta)).append(System.lineSeparator());
            } else {
                builder.append(split[i].trim()).append(System.lineSeparator());
            }
        }
        for (int i = 33; i >= 11; i--) {
            char chars[] = split[i].trim().toCharArray();
            for (int j = chars.length - 1; j >= 0; j--) {
                if (chars[j] == 'A') {
                    builder.append('V');
                } else if (chars[j] == 'V') {
                    builder.append('A');
                } else if (chars[j] == 'i') {
                    builder.append('!');
                } else if (chars[j] == '!') {
                    builder.append('i');
                } else {
                    builder.append(chars[j]);
                }
            }
            builder.append(System.lineSeparator());
        }
        for (int i = 10; i >= 0; i--) {
            if (i == 3) {
                String split2[] = split[i].trim().split("[\\s:]+");
                int delta = Integer.parseInt(split2[3]);
                delta = delta * -1;
                builder.append(String.format("# Delta X: %2d     #", delta)).append(System.lineSeparator());
            } else {
                builder.append(split[i].trim()).append(System.lineSeparator());
            }
        }
        return builder.toString();
    }

    public static Move flipMove(Move move) {
        if (move == Move.LEFT) {
            return Move.RIGHT;
        } else if (move == Move.RIGHT) {
            return Move.LEFT;
        } else {
            return move;
        }
    }

    public static boolean compareStates(String state1, String state2) throws StateComparisonException {
        String split1[] = state1.trim().split("\n");
        String split2[] = state2.trim().split("\n");
        if (split1.length != split2.length) {
            throw new StateComparisonException("Expected state to be " + split1.length + " lines. State was " + split2.length + " lines instead.");
        }
        for (int i = 0; i <= 10; i++) {
            String s1 = split1[i].trim().replaceAll("\\s+", "");
            String s2 = split2[i].trim().replaceAll("\\s+", "");
            if (!s1.equals(s2)) {
                throw new StateComparisonException("State mismatch on line " + (i) + ". Expected [" + split1[i].trim() + "]. Received [" + split2[i] + "].");
            }
        }

        for (int i = 11; i <= 33; i++) {
            if (!split1[i].trim().equals(split2[i].trim())) {
                throw new StateComparisonException("State mismatch on line " + (i) + ". Expected [" + split1[i].trim() + "]. Received [" + split2[i] + "].");
            }
        }
        for (int i = 34; i <= 44; i++) {
            String s1 = split1[i].trim().replaceAll("\\s+", "");
            String s2 = split2[i].trim().replaceAll("\\s+", "");
            if (!s1.equals(s2)) {
                throw new StateComparisonException("State mismatch on line " + (i) + ". Expected [" + split1[i].trim() + "]. Received [" + split2[i] + "].");
            }

        }
        return true;
    }

    public static boolean expectedResult(String state, Move player1Move, Move player2Move, String expectedState) throws IOException, StateComparisonException {
        return expectedResult(state, player1Move, player2Move, expectedState, new LinkedList<>());
    }

    public static boolean expectedResult(String state, Move player1Move, Move player2Move, String expectedState, List<Point2D> bullets) throws IOException, StateComparisonException {
        TestableAgent agent = AgentInitialiser.getTestableAgent();

        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);

        Point2D[] bulletArray = new Point2D[bullets.size()];
        bulletArray = bullets.toArray(bulletArray);
        String result = testable.getNextState(player1Move, player2Move, bulletArray).getMapString();

        System.out.println(result);
        if (!compareStates(expectedState, result)) {
            return false;
        }

        state = flipBoard(state);
        expectedState = flipBoard(expectedState);

        in = new ByteArrayInputStream(state.getBytes());
        testable = agent.getGameState(in);
        bulletArray = flipBullets(bulletArray);

        //moves aren't flipped because moves are given relative to that player being player 1.
        result = testable.getNextState(player2Move, player1Move, bulletArray).getMapString();
        try {
            if (!compareStates(expectedState, result)) {
                return false;
            }
        } catch (StateComparisonException e) {
            throw new StateComparisonException("Comparison test failed with flipped map.", e);
        }
        return true;
    }

    public static void simulate(String state, File outputFolder, Move player1Moves[], Move player2Moves[]) throws IOException {
        TestableAgent agent = AgentInitialiser.getTestableAgent();
        ByteArrayInputStream in = new ByteArrayInputStream(state.getBytes());
        TestableGameState testable = agent.getGameState(in);
        File f = new File(outputFolder.getAbsoluteFile() + "/" + String.format("%03d", 0));
        f.mkdirs();
        File outfile = new File(f.getAbsoluteFile() + "/map-detailed.txt");
        FileWriter fw = new FileWriter(outfile);
        fw.write(testable.getMapString().replaceAll("[\n\r]+", System.lineSeparator()));
        fw.close();
        File move1 = new File(f.getAbsoluteFile() + "/move1.txt");
        fw = new FileWriter(move1);
        fw.write(testable.getPlayer1Name() + ": " + Move.NONE + "              > Move result.");
        fw.close();
        File move2 = new File(f.getAbsoluteFile() + "/move2.txt");
        fw = new FileWriter(move2);
        fw.write(testable.getPlayer2Name() + ": " + Move.NONE + "              > Move result.");
        fw.close();

        for (int i = 0; i < player1Moves.length; i++) {
            testable = testable.getNextState(player1Moves[i], player2Moves[i]);
            f = new File(outputFolder.getAbsoluteFile() + "/" + String.format("%03d", i + 1));
            f.mkdirs();
            outfile = new File(f.getAbsoluteFile() + "/map-detailed.txt");
            fw = new FileWriter(outfile);
            fw.write(testable.getMapString().replaceAll("[\n\r]+", System.lineSeparator()));
            fw.close();
            move1 = new File(f.getAbsoluteFile() + "/move1.txt");
            fw = new FileWriter(move1);
            fw.write(testable.getPlayer1Name() + ": " + player1Moves[i] + "              > Move result.");
            fw.close();
            move2 = new File(f.getAbsoluteFile() + "/move2.txt");
            fw = new FileWriter(move2);
            fw.write(testable.getPlayer2Name() + ": " + player2Moves[i] + "              > Move result.");
            fw.close();
        }
    }
}
