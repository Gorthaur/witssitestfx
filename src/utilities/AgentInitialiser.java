package utilities;

import com.google.common.reflect.ClassPath;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import spaceinvaderstester.Agent;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 *
 * @author Dean
 */
public class AgentInitialiser {

    static List<Constructor> testableConstructors;
    static List<Agent> agents;

    public static TestableAgent getTestableAgent() throws IOException {
        List<TestableAgent> agents = getAllTestableAgents();
        if (agents.isEmpty()) {
            throw new RuntimeException("Unable to find class which implements TestableAgent and has an empty constructor.");
        }
        return agents.get(0);
    }

    public static List<Agent> getAllAgents() throws IOException {
        if (agents != null) {
            return agents;
        }
        agents = new LinkedList<>();
        testableConstructors = new LinkedList<>();

        final ClassLoader loader = Thread.currentThread().getContextClassLoader();

        for (final ClassPath.ClassInfo info : ClassPath.from(loader).getTopLevelClasses()) {
            if (info.getName().startsWith("za.ac.wits.")) {
                final Class<?> clazz = info.load();
                if (!Modifier.isAbstract(clazz.getModifiers())) { //if the class isn't abstract
                    Class[] interfaces = clazz.getInterfaces();
                    for (Class in : interfaces) {
                        if (in.getCanonicalName().equals("za.ac.wits.witstestablespaceinvaders.TestableAgent")) {

                            Constructor constructors[] = clazz.getConstructors();
                            for (Constructor cons : constructors) {
                                Class[] parameterTypes = cons.getParameterTypes();
                                if (parameterTypes.length == 0) {
                                    testableConstructors.add(cons);
                                    agents.add(new Agent(cons));
                                }
                            }
                        }
                    }
                }
            }
        }

        return agents;
    }

    public static List<TestableAgent> getAllTestableAgents() throws IOException {
        List<Agent> agents = getAllAgents();
        LinkedList<TestableAgent> testableAgents = new LinkedList<>();
        for (Agent a : agents) {
            TestableAgent agent = a.getNewInstance();
            testableAgents.add(agent);
        }
        return testableAgents;
    }
}
