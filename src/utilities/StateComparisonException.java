
package utilities;

/**
 *
 * @author Dean
 */
public class StateComparisonException extends Exception {

    public StateComparisonException(String message, Throwable cause) {
        super(message, cause);
    }

    public StateComparisonException(String message) {
        super(message);
    }
}
