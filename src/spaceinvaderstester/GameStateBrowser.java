package spaceinvaderstester;

import java.util.LinkedList;
import java.util.concurrent.Executors;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Dean
 */
public class GameStateBrowser extends Group {

    final static long ANIMATION_TIME = 700;
    final BoardPane board;
    final ScrollBar scrollBar;
    final VBox vBox;
    final HBox buttons;
    final Button playPauseButton;
    final LinkedList<GameState> states;
    ObservableList<GameState> allStates;
    final IntegerProperty numStates;
    final IntegerProperty currentStateIndex;

    protected final ObjectProperty<SpaceInvaderMatch> currentMatch = new SimpleObjectProperty<>();

    public ReadOnlyObjectProperty<SpaceInvaderMatch> currentMatchProperty() {
        return currentMatch;
    }

    protected final ObjectProperty<GameState> gameState;

    public ReadOnlyObjectProperty<GameState> gameStateProperty() {
        return gameState;
    }

    public GameStateBrowser() {
        this.getStylesheets().add(BoardPane.class.getResource("browser.css").toExternalForm());
        vBox = new VBox();
        this.getChildren().add(vBox);
        board = new BoardPane();
        scrollBar = new ScrollBar();
        gameState = new SimpleObjectProperty<>();
        states = new LinkedList<>();
        allStates = FXCollections.observableList(states);

        vBox.getChildren().add(board);
        vBox.getChildren().add(scrollBar);
        buttons = new HBox();
        buttons.setAlignment(Pos.CENTER);
        playPauseButton = new Button("");
        playPauseButton.getStyleClass().add("play");
        playPauseButton.setOnAction((e) -> play());
        buttons.getChildren().add(playPauseButton);
        vBox.getChildren().add(buttons);
        numStates = new SimpleIntegerProperty();

        scrollBar.maxProperty().bind(Bindings.max(0, numStates));
        scrollBar.setBlockIncrement(1);
        scrollBar.setUnitIncrement(1);

        scrollBar.setVisibleAmount(0.5);
        //scrollBar.disableProperty().bind(Bindings.lessThan(numStates, 2));

        IntegerBinding num = new IntegerBinding() {
            {
                bind(scrollBar.valueProperty(), numStates);
            }

            @Override
            protected int computeValue() {
                int val = Math.min((int) Math.floor(scrollBar.valueProperty().doubleValue())-1, numStates.getValue() - 1);
                System.out.println(scrollBar.valueProperty().get() + " " + val);
                System.out.println(scrollBar.getUnitIncrement());
                return val;
            }
        };
        currentStateIndex = new SimpleIntegerProperty();
        currentStateIndex.bind(num);

        /**
         * Keeps value in the range when things get removed
         */
        scrollBar.maxProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            int val = Math.max(0, Math.min((int) scrollBar.getValue(), allStates.size()));
            if (val != (int) scrollBar.getValue()) {
                scrollBar.setValue(val);
                System.out.println(val);
            }
        });

        /**
         * When the current state changes, it gets redrawn
         */
        gameState.addListener((ObservableValue<? extends GameState> observable, GameState oldValue, GameState newValue) -> {
            if (newValue == null) {
                board.clear();
            } else {
                board.transitionState(newValue, ANIMATION_TIME);
            }
        });

        /**
         * Follows the scrollbar
         */
        currentStateIndex.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (Platform.isFxApplicationThread()) {
                    if (allStates.size() <= 0 || newValue.intValue() >= allStates.size() || newValue.intValue() < 0) {
                        gameState.setValue(null);
                    } else {
                        final GameState temp = allStates.get(newValue.intValue());
                        gameState.setValue(temp);
                    }
                } else {
                    Platform.runLater(() -> changed(observable, oldValue, newValue));
                }
            }
        });

    }

    public void setMatch(SpaceInvaderMatch match) {
        if (Platform.isFxApplicationThread()) {
            allStates = match.gameStates;
            currentMatch.setValue(match);
            scrollBar.setValue(0);
            numStates.bind(Bindings.size(allStates));
        } else {
            Platform.runLater(() -> setMatch(match));
        }
    }

    public void addState(GameState g) {
        if (Platform.isFxApplicationThread()) {
            allStates.add(g);
        } else {
            Platform.runLater(() -> {
                addState(g);
            });
        }
    }

    public void play() {
        PlayTask play = new PlayTask();
        Executors.newSingleThreadExecutor().submit(play);
    }

    class PlayTask extends Task<Void> {

        boolean stop = false;

        @Override
        protected Void call() throws Exception {
            EventHandler r = playPauseButton.getOnAction();
            playPauseButton.getStyleClass().remove("play");
            playPauseButton.getStyleClass().add("pause");
            playPauseButton.setOnAction((e) -> {
                this.stop();
            });
            Platform.runLater(() -> scrollBar.setDisable(true));
            while (!stop && scrollBar.getValue() < scrollBar.getMax()) {
                //Platform.runLater(() -> scrollBar.increment());
                scrollBar.increment();
                try {
                    Thread.sleep(ANIMATION_TIME);
                } catch (InterruptedException e) {
                }
            }
            playPauseButton.getStyleClass().remove("pause");
            playPauseButton.getStyleClass().add("play");
                        Platform.runLater(() -> scrollBar.setDisable(false));
            playPauseButton.setOnAction(r);
            return null;
        }

        public void stop() {
            this.stop = true;
            Platform.runLater(() -> scrollBar.setDisable(false));
            this.cancel(true);
        }

    }
}
