package spaceinvaderstester;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 *
 * @author Dean
 */
public class Agent {

    private final Constructor constructor;
    private final TestableAgent defaultInstance;

    public Agent(Constructor c) {
        this.constructor = c;
        defaultInstance = getNewInstance();
    }

    public TestableAgent getDefaultInstance() {
        return defaultInstance;
    }

    public final TestableAgent getNewInstance() {
        try {
            return (TestableAgent) (constructor.newInstance());
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Agent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String toString() {
        if (defaultInstance == null) {
            return "null";
        } else {
            return defaultInstance.getDescription();
        }
    }

}
