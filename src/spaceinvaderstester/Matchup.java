package spaceinvaderstester;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

/**
 *
 * @author Dean
 */
public class Matchup {

    String player1Name;
    String player2Name;
    StringProperty player1NameProperty = new SimpleStringProperty();
    StringProperty player2NameProperty = new SimpleStringProperty();

    ObjectProperty<ObservableList<Series<Number, Number>>> matchKillsProperty = new SimpleObjectProperty<>();
    ObjectProperty<ObservableList<Series<Number, Number>>> matchLivesProperty = new SimpleObjectProperty<>();
    ObservableList<Series<Number, Number>> matchKills = FXCollections.observableList(new LinkedList<>());
    ObservableList<Series<Number, Number>> matchLives = FXCollections.observableList(new LinkedList<>());
    DoubleProperty averageRound = new SimpleDoubleProperty(0);
    IntegerProperty games = new SimpleIntegerProperty(0);
    DoubleProperty player1WinPercentage = new SimpleDoubleProperty(0);
    DoubleProperty player2WinPercentage = new SimpleDoubleProperty(0);
    int p1Wins = 0;
    int p2Wins = 0;
    double p1Kills[] = new double[1000];
    double p2Kills[] = new double[1000];
    double p1Lives[] = new double[1000];
    double p2Lives[] = new double[1000];

    private final Object matchLock = new Object();

    public Matchup(String player1Name, String player2Name, SpaceInvaderMatch match) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
        player1NameProperty.setValue(player1Name);
        player2NameProperty.setValue(player2Name);
        games.setValue(0);
        matchKillsProperty.setValue(matchKills);
        matchLivesProperty.setValue(matchLives);
        Arrays.fill(p1Kills, -1);
        Arrays.fill(p2Kills, -1);
        Arrays.fill(p1Lives, -1);
        Arrays.fill(p2Lives, -1);
        addMatch(match);
    }

    private void addMatchReverse(SpaceInvaderMatch match) {
        int g = games.get();
        double ave = (averageRound.get() * g + match.gameStates.size()) / (g + 1);
        int count = 0;
        if (match.player2Winner) {
            p1Wins++;
        } else if (match.player1Winner) {
            p2Wins++;
        }
        for (GameState gs : match.gameStates) {
            if (p1Kills[count] == -1) {
                p1Kills[count] = gs.player2Kills;
                p2Kills[count] = gs.player1Kills;
                p1Lives[count] = gs.player2Lives;
                p2Lives[count] = gs.player1Lives;
            } else {
                p1Kills[count] = (p1Kills[count] * g + gs.player2Kills) / (g + 1.0);
                p2Kills[count] = (p2Kills[count] * g + gs.player1Kills) / (g + 1.0);
                p1Lives[count] = (p1Lives[count] * g + gs.player2Lives) / (g + 1.0);
                p2Lives[count] = (p2Lives[count] * g + gs.player1Lives) / (g + 1.0);
            }
            count++;
        }
        GameState gs = match.gameStates.get(match.gameStates.size() - 1);
        for (int i = count; i < 1000; i++) {
            if (p1Kills[i] == -1) {
                p1Kills[i] = gs.player2Kills;
                p2Kills[i] = gs.player1Kills;
                p1Lives[i] = gs.player2Lives;
                p2Lives[i] = gs.player1Lives;
            } else {
                p1Kills[i] = (p1Kills[i] * g + gs.player2Kills) / (g + 1.0);
                p2Kills[i] = (p2Kills[i] * g + gs.player1Kills) / (g + 1.0);
                p1Lives[i] = (p1Lives[i] * g + gs.player2Lives) / (g + 1.0);
                p2Lives[i] = (p2Lives[i] * g + gs.player1Lives) / (g + 1.0);
            }
        }
        XYChart.Series player1Kills = new XYChart.Series();
        XYChart.Series player2Kills = new XYChart.Series();
        XYChart.Series player1Lives = new XYChart.Series();
        XYChart.Series player2Lives = new XYChart.Series();
        player1Kills.setName(player1Name + " Kills");
        player2Kills.setName(player2Name + " Kills");
        player1Lives.setName(player1Name + " Lives");
        player2Lives.setName(player2Name + " Lives");
        for (int i = 0; i < p1Kills.length; i++) {
            player1Kills.getData().add(new XYChart.Data(i, p1Kills[i]));
            player2Kills.getData().add(new XYChart.Data(i, p2Kills[i]));
            player1Lives.getData().add(new XYChart.Data(i, p1Lives[i]));
            player2Lives.getData().add(new XYChart.Data(i, p2Lives[i]));
        }
        double p1Percentage = (p1Wins * 100.0) / (p1Wins + p2Wins);
        double p2Percentage = (p2Wins * 100.0) / (p1Wins + p2Wins);
        if (!Double.isFinite(p1Percentage)) {
            p1Percentage = 0;
        }
        if (!Double.isFinite(p2Percentage)) {
            p2Percentage = 0;
        }
        updateMatchProperties(g + 1, ave, p1Percentage, p2Percentage, player1Kills, player2Kills, player1Lives, player2Lives);
    }

    public void addMatch(SpaceInvaderMatch match) {
        synchronized (matchLock) {
            if (!match.player1Name.equals(player1Name) && match.player1Name.equals(match.player2Name)) {
                addMatchReverse(match);
                return;
            }
            int g = games.get();
            double ave = (averageRound.get() * g + match.gameStates.size()) / (g + 1);
            int count = 0;
            if (match.player1Winner) {
                p1Wins++;
            } else if (match.player2Winner) {
                p2Wins++;
            }
            for (GameState gs : match.gameStates) {
                if (p1Kills[count] == -1) {
                    p1Kills[count] = gs.player1Kills;
                    p2Kills[count] = gs.player2Kills;
                    p1Lives[count] = gs.player1Lives;
                    p2Lives[count] = gs.player2Lives;
                } else {
                    p1Kills[count] = (p1Kills[count] * g + gs.player1Kills) / (g + 1.0);
                    p2Kills[count] = (p2Kills[count] * g + gs.player2Kills) / (g + 1.0);
                    p1Lives[count] = (p1Lives[count] * g + gs.player1Lives) / (g + 1.0);
                    p2Lives[count] = (p2Lives[count] * g + gs.player2Lives) / (g + 1.0);
                }
                count++;
            }
            GameState gs = match.gameStates.get(match.gameStates.size() - 1);
            for (int i = count; i < 1000; i++) {
                if (p1Kills[i] == -1) {
                    p1Kills[i] = gs.player1Kills;
                    p2Kills[i] = gs.player2Kills;
                    p1Lives[i] = gs.player1Lives;
                    p2Lives[i] = gs.player2Lives;
                } else {
                    p1Kills[i] = (p1Kills[i] * g + gs.player1Kills) / (g + 1.0);
                    p2Kills[i] = (p2Kills[i] * g + gs.player2Kills) / (g + 1.0);
                    p1Lives[i] = (p1Lives[i] * g + gs.player1Lives) / (g + 1.0);
                    p2Lives[i] = (p2Lives[i] * g + gs.player2Lives) / (g + 1.0);
                }
            }
            XYChart.Series player1Kills = new XYChart.Series();
            XYChart.Series player2Kills = new XYChart.Series();
            XYChart.Series player1Lives = new XYChart.Series();
            XYChart.Series player2Lives = new XYChart.Series();
            player1Kills.setName(player1Name + " Kills");
            player2Kills.setName(player2Name + " Kills");
            player1Lives.setName(player1Name + " Lives");
            player2Lives.setName(player2Name + " Lives");
            for (int i = 0; i < p1Kills.length; i++) {
                player1Kills.getData().add(new XYChart.Data(i, p1Kills[i]));
                player2Kills.getData().add(new XYChart.Data(i, p2Kills[i]));
                player1Lives.getData().add(new XYChart.Data(i, p1Lives[i]));
                player2Lives.getData().add(new XYChart.Data(i, p2Lives[i]));
            }
            double p1Percentage = (p1Wins * 100.0) / (p1Wins + p2Wins);
            double p2Percentage = (p2Wins * 100.0) / (p1Wins + p2Wins);
            if (!Double.isFinite(p1Percentage)) {
                p1Percentage = 0;
            }
            if (!Double.isFinite(p2Percentage)) {
                p2Percentage = 0;
            }
            updateMatchProperties(g + 1, ave, p1Percentage, p2Percentage, player1Kills, player2Kills, player1Lives, player2Lives);
        }
    }

    private void updateMatchProperties(int numGames, double averageRound, double p1Win, double p2Win, XYChart.Series player1Kills, XYChart.Series player2Kills, XYChart.Series player1Lives, XYChart.Series player2Lives) {
        if (Platform.isFxApplicationThread()) {
            games.setValue(numGames);
            this.averageRound.setValue(averageRound);
            matchKills.clear();
            matchKills.addAll(player1Kills, player2Kills);
            matchLives.clear();
            matchLives.addAll(player1Lives, player2Lives);
            player1WinPercentage.setValue(p1Win);
            player2WinPercentage.setValue(p2Win);
        } else {
            Platform.runLater(() -> updateMatchProperties(numGames, averageRound, p1Win, p2Win, player1Kills, player2Kills, player1Lives, player2Lives));
        }
    }

    @Override
    public String toString() {
        return player1Name + " vs " + player2Name;
    }

}
