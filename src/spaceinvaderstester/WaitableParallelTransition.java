package spaceinvaderstester;

import com.sun.javafx.tk.Toolkit;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.ParallelTransition;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Node;
import javafx.util.Duration;

/**
 * Have to use decorator pattern.
 *
 * @author Dean
 */
public class WaitableParallelTransition {

    ParallelTransition p;
    Task waitingTask;
    Runnable onFinish;

    public WaitableParallelTransition(Node node, Animation... children) {
        p = new ParallelTransition(node, children);
    }

    public WaitableParallelTransition(Animation... children) {
        p = new ParallelTransition(children);
    }

    public WaitableParallelTransition(Node node) {
        p = new ParallelTransition(node);
    }

    public WaitableParallelTransition() {
        p = new ParallelTransition();
    }

    public void playFromStart() {
/*
        waitingTask = new Task() {

            @Override
            protected Object call() throws Exception {
                if (onFinish != null) {
                    onFinish.run();
                }
                return null;
            }

        };
        p.setOnFinished((e) -> waitingTask.run());
        */
        p.playFromStart();
    }

    public void stop() {
        p.stop();
    }

    public void pause() {
        p.pause();
    }

    public void finish() {
        //p.jumpTo(Duration.millis(1000000)); //end
        p.pause();
        p.setOnFinished(null);
        if (p.currentTimeProperty().get().toMillis() < p.getCycleDuration().toMillis()) {
            p.jumpTo(Duration.millis(p.getCycleDuration().toMillis() - 1));
            p.play();
        }
        if (onFinish != null) {
            onFinish.run();
        }
        /*
         try {
         waitingTask.get();
         } catch (InterruptedException ex) {
         Logger.getLogger(WaitableParallelTransition.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ExecutionException ex) {
         Logger.getLogger(WaitableParallelTransition.class.getName()).log(Level.SEVERE, null, ex);
         }
         Platform.runLater(() -> Toolkit.getToolkit().firePulse());
         */
    }

    void addAll(LinkedList<Animation> transitions) {
        p.getChildren().addAll(transitions);
    }

}
