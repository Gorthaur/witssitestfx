/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaderstester;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Dean
 */
public class StatsPaneController implements Initializable {

    @FXML
    private Label lblP2Kills;
    @FXML
    private Label lblP2Lives;
    @FXML
    private Label lblP2Wave;
    @FXML
    private Label lblP2Missiles;
    @FXML
    private Label lblPlayer2;
    @FXML
    private Label lblP2Energy;
    @FXML
    private Label lblP2Direction;
    @FXML
    private Label lblP1Kills;
    @FXML
    private Label lblP1Lives;
    @FXML
    private Label lblP1Wave;
    @FXML
    private Label lblP1Missiles;
    @FXML
    private Label lblPlayer1;
    @FXML
    private Label lblP1Energy;
    @FXML
    private Label lblP1Direction;
    @FXML
    private Label lblRound;

    private GameStateBrowser browser;
    @FXML
    private VBox vboxHolder;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void monitor(GameStateBrowser browser) {
        ChangeListener<Bounds> boundsListener = new ChangeListener<Bounds>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) {
                updatePosition();
            }

        };
        ChangeListener<Number> numberListener = new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                updatePosition();
            }

        };
        this.browser = browser;
        updatePosition();
        browser.board.layoutBoundsProperty().addListener(boundsListener);
        vboxHolder.layoutBoundsProperty().addListener(boundsListener);
        browser.board.boundsInParentProperty().addListener(boundsListener);
        if (browser.board.sceneProperty().get() == null) {
            browser.board.sceneProperty().addListener(new ChangeListener<Scene>() {

                @Override
                public void changed(ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) {
                    if (newValue != null) {
                        browser.board.sceneProperty().get().widthProperty().addListener(numberListener);
                        browser.board.sceneProperty().get().heightProperty().addListener(numberListener);
                    }
                }

            });
        }

        browser.gameStateProperty().addListener(new ChangeListener<GameState>() {

            @Override
            public void changed(ObservableValue<? extends GameState> observable, GameState oldValue, GameState newValue) {
                if (newValue != null) {
                    updateBindings(newValue);
                } else {
                    updateBindings(new GameState());
                }
            }

        });
    }

    public void updatePosition() {
        Bounds local = browser.board.getLayoutBounds();

        Point2D middle = new Point2D((local.getMaxX() - local.getMinX()) / 2.0, (local.getMaxY() - local.getMinY()) / 2.0);
        Point2D board = browser.board.localToScene(middle, true);

        Point2D vBoxLocal = vboxHolder.sceneToLocal(board);
        Point2D parent = vboxHolder.localToParent(vBoxLocal);
        if (local == null) {
            return;
        }
        vboxHolder.setTranslateY(vBoxLocal.getY() + vboxHolder.getTranslateY() - (vboxHolder.getHeight() / 2.0));
    }

    private void updateBindings(GameState gameState) {
        lblP2Kills.textProperty().bind(gameState.player2KillsProperty());
        lblP2Lives.textProperty().bind(gameState.player2LivesProperty());
        lblP2Wave.textProperty().bind(gameState.player2WaveSizeProperty());
        lblPlayer2.textProperty().bind(gameState.player2NameProperty());
        lblP2Missiles.textProperty().bind(gameState.player2MissilesProperty());
        lblP2Energy.textProperty().bind(gameState.player2ShotEnergyProperty());
        lblP2Direction.textProperty().bind(gameState.player2DirectionProperty());
        lblP1Kills.textProperty().bind(gameState.player1KillsProperty());
        lblP1Lives.textProperty().bind(gameState.player1LivesProperty());
        lblP1Wave.textProperty().bind(gameState.player1WaveSizeProperty());
        lblPlayer1.textProperty().bind(gameState.player1NameProperty());
        lblP1Missiles.textProperty().bind(gameState.player1MissilesProperty());
        lblP1Energy.textProperty().bind(gameState.player1ShotEnergyProperty());
        lblP1Direction.textProperty().bind(gameState.player1DirectionProperty());
        lblRound.textProperty().bind(gameState.roundProperty);
    }

}
