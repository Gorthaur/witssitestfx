package spaceinvaderstester;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Point2D;
import za.ac.wits.witstestablespaceinvaders.SpaceInvaderObject;

/**
 *
 * @author Dean
 */
public class GameState {

    SimpleStringProperty roundProperty = new SimpleStringProperty("0");
    public int player2Delta;
    public int player1Delta;

    public ReadOnlyStringProperty roundProperty() {
        return roundProperty;
    }
    SimpleStringProperty mapProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty mapProperty() {
        return mapProperty;
    }
    SimpleStringProperty heuristicProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty heuristicProperty() {
        return heuristicProperty;
    }
    SimpleStringProperty stateDescriptionProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty stateDescriptionProperty() {
        return stateDescriptionProperty;
    }
    SimpleStringProperty player1LogsProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1LogsProperty() {
        return player1LogsProperty;
    }
    SimpleStringProperty player2LogsProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2LogsProperty() {
        return player2LogsProperty;
    }

    SimpleStringProperty player2KillsProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2KillsProperty() {
        return player2KillsProperty;
    }

    SimpleStringProperty player2MissilesProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2MissilesProperty() {
        return player2MissilesProperty;
    }
    SimpleStringProperty player2ShotEnergyProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2ShotEnergyProperty() {
        return player2ShotEnergyProperty;
    }

    SimpleStringProperty player2NameProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2NameProperty() {
        return player2NameProperty;
    }

    SimpleStringProperty player2LivesProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2LivesProperty() {
        return player2LivesProperty;
    }
    SimpleStringProperty player2WaveSizeProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2WaveSizeProperty() {
        return player2WaveSizeProperty;
    }

    SimpleStringProperty player2DirectionProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2DirectionProperty() {
        return player2DirectionProperty;
    }

    SimpleStringProperty player1KillsProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1KillsProperty() {
        return player1KillsProperty;
    }

    SimpleStringProperty player1MissilesProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1MissilesProperty() {
        return player1MissilesProperty;
    }
    SimpleStringProperty player1ShotEnergyProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1ShotEnergyProperty() {
        return player1ShotEnergyProperty;
    }

    SimpleStringProperty player1NameProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1NameProperty() {
        return player1NameProperty;
    }

    SimpleStringProperty player1LivesProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1LivesProperty() {
        return player1LivesProperty;
    }
    SimpleStringProperty player1WaveSizeProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1WaveSizeProperty() {
        return player1WaveSizeProperty;
    }

    SimpleStringProperty player1DirectionProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1DirectionProperty() {
        return player1DirectionProperty;
    }
    SimpleStringProperty stateStringProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty stateStringProperty() {
        return stateStringProperty;
    }

    SimpleStringProperty player1LastMoveProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1LastMoveProperty() {
        return player1LastMoveProperty;
    }

    SimpleStringProperty player2LastMoveProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2LastMoveProperty() {
        return player2LastMoveProperty;
    }

    SimpleStringProperty player1LastMoveResultProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player1LastMoveResultProperty() {
        return player1LastMoveResultProperty;
    }

    SimpleStringProperty player2LastMoveResultProperty = new SimpleStringProperty("");

    public ReadOnlyStringProperty player2LastMoveResultProperty() {
        return player2LastMoveResultProperty;
    }

    int round = 0;
    String player1Name;
    String player2Name;
    String player1Logs;
    String player2Logs;

    int player1RightmostAlienX = -1;
    int player2RightmostAlienX = -1;
    int player1LeftmostAlienX = 23;
    int player2LeftmostAlienX = 23;
    
    int player1Lives;
    int player2Lives;
    int player1Kills;
    int player2Kills;
    
    boolean player1ShipAlive = false;
    boolean player2ShipAlive = false;
    

    int player1ShotEnergy;
    int player2ShotEnergy;

    SpaceInvaderObject board[][] = new SpaceInvaderObject[17][23];

    List<Point2D> aliens = new LinkedList<>();
    List<Point2D> ships = new LinkedList<>();
    List<Point2D> bullets = new LinkedList<>();
    List<Point2D> missilesUp = new LinkedList<>();
    List<Point2D> missilesDown = new LinkedList<>();
    List<Point2D> alienFactories = new LinkedList<>();
    List<Point2D> shields = new LinkedList<>();
    List<Point2D> missileFactories = new LinkedList<>();

    public GameState(String mapFilePath) throws FileNotFoundException, IOException {
        File f = new File(mapFilePath);
        BufferedReader reader = new BufferedReader(new FileReader(f));
        //Player player1 = readPlayer(reader);
        StringBuilder builder = new StringBuilder();
        readPlayer2(reader, builder);
        readMap(reader, builder);
        readPlayer1(reader, builder);
        this.stateStringProperty.setValue(builder.toString());
    }

    
    public GameState() {
        
    }
    
    
    public void setMovesTaken(File move1, File move2) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(move1));
        String line = reader.readLine().trim();
        String split[] = line.split(":");
        String split2[] = split[1].trim().split("\\s+");
        String move = split2[0];
        String result = split[1].trim().split(">")[1].trim();
        player1LastMoveProperty.setValue(move);
        player1LastMoveResultProperty.setValue(result);

        reader = new BufferedReader(new FileReader(move2));
        line = reader.readLine().trim();
        split = line.split(":");
        split2 = split[1].trim().split("\\s+");
        move = split2[0];
        result = split[1].trim().split(">")[1].trim();
        player2LastMoveProperty.setValue(move);
        player2LastMoveResultProperty.setValue(result);

    }

    public void readMap(BufferedReader reader, StringBuilder builder) throws IOException {
        for (int i = 0; i < 23; i++) {
            String line = reader.readLine().trim();
            builder.append(line).append(System.lineSeparator());
            line = line.substring(1, line.length() - 1);
            char chars[] = line.toCharArray();
            int j = 0;
            while (j < chars.length) {
                if (chars[j] == 'M') {
                    missileFactories.add(new Point2D(j + 1, i));
                    board[j + 1][i] = SpaceInvaderObject.MISSILE_FACTORY;
                    j += 2;
                } else if (chars[j] == 'X') {
                    alienFactories.add(new Point2D(j + 1, i));
                    board[j + 1][i] = SpaceInvaderObject.ALIEN_FACTORY;
                    j += 2;
                } else if (chars[j] == 'V') {
                    ships.add(new Point2D(j + 1, i));
                    board[j + 1][i] = SpaceInvaderObject.SHIP;
                                        player2ShipAlive = true;
                    j += 2;
                } else if (chars[j] == 'A') {
                    ships.add(new Point2D(j + 1, i));
                    player1ShipAlive = true;
                    board[j + 1][i] = SpaceInvaderObject.SHIP;
                    j += 2;
                } else if (chars[j] == '|') {
                    bullets.add(new Point2D(j, i));
                    board[j][i] = SpaceInvaderObject.ALIEN_MISSILE;
                } else if (chars[j] == '-') {
                    shields.add(new Point2D(j, i));
                    board[j][i] = SpaceInvaderObject.SHIELD;
                } else if (chars[j] == 'i') {
                    missilesDown.add(new Point2D(j, i));
                    board[j][i] = SpaceInvaderObject.PLAYER2_MISSILE;
                } else if (chars[j] == '!') {
                    missilesUp.add(new Point2D(j, i));
                    board[j][i] = SpaceInvaderObject.PLAYER1_MISSILE;
                } else if (chars[j] == 'x') {
                    if (i <= 11) {
                        player2LeftmostAlienX = Math.min(j, player2LeftmostAlienX);
                        player2RightmostAlienX = Math.max(j, player2RightmostAlienX);
                    } else {
                        player1LeftmostAlienX = Math.min(j, player1LeftmostAlienX);
                        player1RightmostAlienX = Math.max(j, player1RightmostAlienX);
                    }
                    aliens.add(new Point2D(j, i));
                    board[j][i] = SpaceInvaderObject.ALIEN;
                }
                j++;
            }
        }
    }

    public void readPlayer(BufferedReader reader, StringBuilder builder) throws IOException {
        String line = reader.readLine();
        builder.append(line).append(System.lineSeparator());
        line = reader.readLine();
        while (!line.equals("###################")) {
            builder.append(line).append(System.lineSeparator());
            if (line.contains("Round:")) {
                String split[] = line.split("\\s+");
                this.round = Integer.parseInt(split[2]);
            }
            line = reader.readLine();

        }
        builder.append(line).append(System.lineSeparator());
    }

    public void readPlayer1(BufferedReader reader, StringBuilder builder) throws IOException {
        String line = reader.readLine();
        builder.append(line).append(System.lineSeparator());
        line = reader.readLine();
        while (!line.trim().equals("###################")) {
            builder.append(line).append(System.lineSeparator());
            if (line.contains("Round:")) {
                String split[] = line.split("\\s+");
                this.round = Integer.parseInt(split[2]);
                this.roundProperty.setValue(String.valueOf(round));
            } else if (line.contains("Kills")) {
                String split[] = line.split("\\s+");
                int kills = Integer.parseInt(split[2]);
                this.player1KillsProperty.setValue(String.valueOf(kills));
                this.player1Kills = kills;
            } else if (line.contains("Lives")) {
                String split[] = line.split("\\s+");
                int lives = Integer.parseInt(split[2]);
                this.player1LivesProperty.setValue(String.valueOf(lives));
                player1Lives = lives;
            } else if (line.contains("Missiles")) {
                String split[] = line.split("\\s+");
                this.player1MissilesProperty.setValue(split[2]);
            } else if (line.contains("Energy")) {
                String split[] = line.split("\\s+");
                String split2[] = split[2].split("/");
                this.player1ShotEnergyProperty.setValue(split[2]);
                player1ShotEnergy = Integer.parseInt(split2[0].trim());
            } else if (line.contains("Missiles")) {
                String split[] = line.split("\\s+");
                this.player1MissilesProperty.setValue(split[2]);
            } else if (line.contains("Wave")) {
                String split[] = line.split("\\s+");
                this.player1WaveSizeProperty.setValue(split[3]);
            } else if (line.contains("Delta")) {
                String split[] = line.split("\\s+");
                this.player1DirectionProperty.setValue(split[3]);
                player1Delta = Integer.parseInt(split[3]);
            } else if (line.contains("Respawn")) {

            } else {
                this.player1NameProperty.setValue(line.substring(1, line.length() - 1).trim());
                player1Name = player1NameProperty().get();
            }
            line = reader.readLine();

        }
        builder.append(line).append(System.lineSeparator());
    }

    public void readPlayer2(BufferedReader reader, StringBuilder builder) throws IOException {
        String line = reader.readLine();
        builder.append(line).append(System.lineSeparator());
        line = reader.readLine();
        while (!line.trim().equals("###################")) {
            builder.append(line).append(System.lineSeparator());
            if (line.contains("Round:")) {
                String split[] = line.split("\\s+");
                this.round = Integer.parseInt(split[2]);
                this.roundProperty.setValue(String.valueOf(round));
            } else if (line.contains("Kills")) {
                String split[] = line.split("\\s+");
                int kills = Integer.parseInt(split[2]);
                this.player2KillsProperty.setValue(String.valueOf(kills));
                player2Kills = kills;
            } else if (line.contains("Lives")) {
                String split[] = line.split("\\s+");
                int lives = Integer.parseInt(split[2]);
                this.player2LivesProperty.setValue(String.valueOf(lives));
                player2Lives = lives;
            } else if (line.contains("Missiles")) {
                String split[] = line.split("\\s+");
                this.player2MissilesProperty.setValue(split[2]);
            } else if (line.contains("Energy")) {
                String split[] = line.split("\\s+");
                this.player2ShotEnergyProperty.setValue(split[2]);
                String split2[] = split[2].split("/");
                player2ShotEnergy = Integer.parseInt(split2[0].trim());
            } else if (line.contains("Missiles")) {
                String split[] = line.split("\\s+");
                this.player2MissilesProperty.setValue(split[2]);
            } else if (line.contains("Wave")) {
                String split[] = line.split("\\s+");
                this.player2WaveSizeProperty.setValue(split[3]);
            } else if (line.contains("Delta")) {
                String split[] = line.split("\\s+");
                this.player2DirectionProperty.setValue(split[3]);
                player2Delta = Integer.parseInt(split[3]);
            } else if (line.contains("Respawn")) {

            } else {
                this.player2NameProperty.setValue(line.substring(1, line.length() - 1).trim());
                player2Name = player2NameProperty().get();
            }
            line = reader.readLine();

        }
        builder.append(line).append(System.lineSeparator());
    }

    public SpaceInvaderObject[][] getSpaceInvaderBoard() {
        return board;
    }

    public List<Point2D> getAliens() {
        return aliens;
    }

    public List<Point2D> getBullets() {
        return bullets;
    }

    public List<Point2D> getMissilesUp() {
        return missilesUp;
    }

    public List<Point2D> getMissilesDown() {
        return missilesDown;
    }

    public List<Point2D> getShips() {
        return ships;
    }

    public List<Point2D> getAlienFactories() {
        return alienFactories;
    }

    public List<Point2D> getShields() {
        return shields;
    }

    public List<Point2D> getMissileFactories() {
        return missileFactories;
    }

}
