/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaderstester;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;


public class GameStateLoader {

    public GameStateLoader() {

    }

    public Task<SpaceInvaderMatch> loadGameStates(File directory) {
        Task<SpaceInvaderMatch> t = new Task<SpaceInvaderMatch>() {

            @Override
            protected SpaceInvaderMatch call() throws Exception {
                LinkedList<GameState> states = new LinkedList<>();
                File replayFolder = directory;

                if (replayFolder.isDirectory()) {
                    String[] gameStateFolderNames = replayFolder.list();
                    int count = 0;
                    for (String gameStateFolderName : gameStateFolderNames) {
                        this.updateProgress(count, gameStateFolderNames.length);
                        count++;
                        File gameStateFolder = new File(replayFolder.getCanonicalPath() + "/" + gameStateFolderName);

                        if (gameStateFolder.isDirectory()) {
                            File gameStateFile = new File(gameStateFolder + "/map-detailed.txt");
                            File move1 = new File(gameStateFolder + "/move1.txt");
                            File move2 = new File(gameStateFolder + "/move2.txt");
                            if (gameStateFile.exists()) {
                                GameState g = new GameState(gameStateFile.getAbsolutePath());
                                if (move1.exists() || move2.exists()) {
                                    g.setMovesTaken(move1, move2);
                                }
                                states.add(g);
                            }
                        }
                    }
                }
                return new SpaceInvaderMatch(replayFolder.getName(), states);
            }

        };

        return t;
    }

    public Task<List<SpaceInvaderMatch>> loadMatches(final File folder) {
        Task<List<SpaceInvaderMatch>> task = new Task<List<SpaceInvaderMatch>>() {

            @Override
            protected List<SpaceInvaderMatch> call() throws Exception {
                LinkedList<SpaceInvaderMatch> matches = new LinkedList<>();
                if (folder.isDirectory()) {
                    File[] replays = folder.listFiles();
                    for (File f : replays) {
                        if (f.isDirectory()) {
                            Task<SpaceInvaderMatch> matchTask = loadGameStates(f);
                            matchTask.progressProperty().addListener(new ChangeListener<Number>() {
                                @Override
                                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                                    updateProgress(matchTask.getWorkDone(), matchTask.getTotalWork());
                                }
                            });
                            try {
                                matchTask.run();
                                SpaceInvaderMatch m = matchTask.get();
                                if (m != null) {
                                    matches.add(m);
                                }
                            } catch (Exception e) {

                            }
                        }
                    }
                }
                return matches;
            }

        };
        return task;
    }

    private GameState getGameState(String gameStateFolderPath) throws IOException {
        File gameStateFolder = new File(gameStateFolderPath);

        if (!gameStateFolder.isDirectory()) {
            return null;
        }

        File gameStateFile = new File(gameStateFolder + "/map-detailed.txt");
        if (!gameStateFile.exists()) {
            return null;
        }

        File move1 = new File(gameStateFolder + "/move1.txt");
        File move2 = new File(gameStateFolder + "/move2.txt");

        GameState g = new GameState(gameStateFile.getAbsolutePath());
        if (move1.exists() || move2.exists()) {
            g.setMovesTaken(move1, move2);
        }
        return g;
    }

    public void monitorFolderForGameStates(GameStateBrowser browser, String replayDirectory) throws IOException {
        WatchService watcher = FileSystems.getDefault().newWatchService();
        File directory = new File(replayDirectory);
        Path dir = directory.toPath();
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_MODIFY);

        File replayFolder = null;

        while (true) {
            try {
                key = watcher.take();
            } catch (InterruptedException ex) {
                Logger.getLogger(GameStateLoader.class.getName()).log(Level.SEVERE, null, ex);
            }

            for (WatchEvent<?> event : key.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();

                if (kind == OVERFLOW) {
                    Logger.getLogger(GameStateLoader.class.getName()).log(Level.SEVERE, null, "Overflow!!!");
                    continue;
                }

                WatchEvent<Path> ev = (WatchEvent<Path>) event;
                Path filename = ev.context();

                if (kind == ENTRY_CREATE) {
                    replayFolder = new File(replayDirectory + "/" + filename);

                    if (!replayFolder.isDirectory()) {
                        return;
                    }

                    System.out.println("File Created: #" + filename + "#");
                } else if (kind == ENTRY_MODIFY) {

                    String[] gameStateFolderNames = replayFolder.list();

                    String lastGameStateFolderName = replayFolder.getCanonicalPath() + "/" + gameStateFolderNames[gameStateFolderNames.length - 1];

//					System.out.println("lastGameStateFolderName: " + lastGameStateFolderName);
                    File lastGameStateFolder = new File(lastGameStateFolderName);

                    if (!lastGameStateFolder.isDirectory()) {
                        continue;
                    }

//					System.out.println("Past continue");
//					Thread.sleep(10);
                    //					WatchService gameStateWatcher = FileSystems.getDefault().newWatchService();
//					
//					lastGameStateFolder.toPath().register(gameStateWatcher, ENTRY_CREATE, ENTRY_MODIFY);
//					WatchKey mapDetailedkey = null;
//					boolean mapDetailedFound = false;
//					while (!mapDetailedFound){
//						System.out.println("sub while");
//
//						try {
//							System.out.println("waiting to take key");
//							mapDetailedkey = gameStateWatcher.take();
//							System.out.println("took key");
//						} catch (InterruptedException ex) {
//							Logger.getLogger(GameStateLoader.class.getName()).log(Level.SEVERE, null, ex);
//						}
//
//						for (WatchEvent<?> subevent: mapDetailedkey.pollEvents()) {
//
//							WatchEvent<Path> subev = (WatchEvent<Path>) subevent;
//							Path createdFile = subev.context();
//
//							if (kind == ENTRY_CREATE){
//								System.out.println("File Created: #" + createdFile + "#");
//							} else {
//								System.out.println("File modified: #" + createdFile + "#");
//							}
//							
//							if (createdFile.equals("map-detailed.txt")){
//								mapDetailedFound = true;
//							}
//						}
//						
//						boolean valid = mapDetailedkey.reset();
//						if (!valid) {
//							System.out.println("map detailed key not valid");;
//						}
//						
//					}
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(GameStateLoader.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //addGameStateToBrowser(lastGameStateFolderName, browser);
                    System.out.println("File Modify: #" + filename + "#");
                }

                boolean valid = key.reset();
                if (!valid) {
                    break;
                }
            }
        }

//		File replayFolder = new File(folderName);
//
//        if (!replayFolder.isDirectory()) return;
//		
//		String[] gameStateFolderNames = replayFolder.list();
//
//		for (String gameStateFolderName : gameStateFolderNames) {
//			addGameStateToBrowser(replayFolder.getCanonicalPath() + "/" + gameStateFolderName, browser);
//		}
    }
}
