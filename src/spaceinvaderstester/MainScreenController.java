package spaceinvaderstester;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import spaceinvaderstester.harness.TestHarness;
import utilities.AgentInitialiser;
import utilities.MoveSimulator;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 * FXML Controller class
 *
 * @author Dean
 */
public class MainScreenController implements Initializable {

    @FXML
    private Button btnLoadReplay;
    //@FXML
    // private Button btnLiveMonitor;

    HashMap<String, Matchup> matchups = new HashMap<>();
    private final Object matchupLock = new Object();

    GameStateBrowser browser = new GameStateBrowser();
    VBox statsPane;
    StatsPaneController statsController;
    @FXML
    private HBox hbxLeft;
    @FXML
    private HBox hboxRight;
    @FXML
    private TextArea txtGameState;
    @FXML
    private Button btnTestCases;

    DeveloperPane developer;
    @FXML
    private VBox vboxCentre;
    @FXML
    private ComboBox<SpaceInvaderMatch> cmbxCurrentReplay;
    @FXML
    private LineChart<Number, Number> chartKills;
    @FXML
    private LineChart<Number, Number> chartLives;
    @FXML
    private NumberAxis axisRoundKills;
    @FXML
    private NumberAxis axisRoundLives;
    @FXML
    private ProgressBar progressLoad;
    @FXML
    private TextField textRounds;
    @FXML
    private ProgressBar progressRoundRobin;
    @FXML
    private CheckBox checkBoxPlaySelf;
    @FXML
    private ListView<Matchup> lstMatchup;
    @FXML
    private Label lblRoundRobinProgress;
    @FXML
    private LineChart<Number, Number> chartAverageKills;
    @FXML
    private LineChart<Number, Number> chartAverageLives;
    @FXML
    private Label lblGames;
    @FXML
    private Label p1Name;
    @FXML
    private Label lblP1WinPercentage;
    @FXML
    private Label p2Name;
    @FXML
    private Label lblAverageRound;
    @FXML
    private Label lblP2WinPercentage;
    @FXML
    private Button btnBatchLoad;
    @FXML
    private ProgressBar progressTestCases;
    @FXML
    private Button btnRoundRobin;
    @FXML
    private Button btnCancelTournament;
    @FXML
    private ComboBox<Agent> cmbAgent1;
    @FXML
    private ComboBox<Agent> cmbAgent2;
    @FXML
    private ProgressBar progressBattle;
    @FXML
    private TextField textRoundsBattle;
    //@FXML
    //private CheckBox checkBoxWatch;
    @FXML
    private Button btnBattle;
    @FXML
    private Label lblBattleProgress;
    @FXML
    private Button btnCancelBattle;
    DirectoryChooser chooser;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("StatsPane.fxml"));
            statsPane = (VBox) loader.load();
            statsController = loader.<StatsPaneController>getController();
            statsController.monitor(browser);
            chartKills.getStyleClass().add("transparent-chart");
            chartLives.getStyleClass().add("transparent-chart");
            vboxCentre.getChildren().add(browser);
            hbxLeft.getChildren().add(statsPane);
            browser.gameStateProperty().addListener(new ChangeListener<GameState>() {

                @Override
                public void changed(ObservableValue<? extends GameState> observable, GameState oldValue, GameState newValue) {
                    if (newValue != null) {
                        txtGameState.textProperty().bind(newValue.stateStringProperty());
                    }
                }

            });
        } catch (IOException ex) {
            Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }

        cmbxCurrentReplay.setOnAction((e) -> {
            browser.setMatch(cmbxCurrentReplay.getValue());
            this.chartKills.getData().clear();
            this.chartKills.getData().add(browser.currentMatchProperty().get().player1Kills);
            this.chartKills.getData().add(browser.currentMatchProperty().get().player2Kills);

            this.chartLives.getData().clear();
            this.chartLives.getData().add(browser.currentMatchProperty().get().player1Lives);
            this.chartLives.getData().add(browser.currentMatchProperty().get().player2Lives);
            axisRoundKills.setAutoRanging(false);
            axisRoundKills.upperBoundProperty().bind(browser.currentStateIndex);
            axisRoundLives.setAutoRanging(false);
            axisRoundLives.upperBoundProperty().bind(browser.currentStateIndex);
        });

        lstMatchup.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Matchup>() {

            @Override
            public void changed(ObservableValue<? extends Matchup> observable, Matchup oldValue, Matchup newValue) {
                if (newValue != null) {
                    chartAverageKills.dataProperty().bind(newValue.matchKillsProperty);
                    chartAverageLives.dataProperty().bind(newValue.matchLivesProperty);
                    lblGames.textProperty().bind(newValue.games.asString());
                    lblAverageRound.textProperty().bind(newValue.averageRound.asString("%.2f"));
                    p1Name.textProperty().bind(Bindings.concat(newValue.player1NameProperty, " Win Percentage: "));
                    p2Name.textProperty().bind(Bindings.concat(newValue.player2NameProperty, " Win Percentage: "));
                    lblP1WinPercentage.textProperty().bind(Bindings.concat(newValue.player1WinPercentage.asString("%.2f"), "%"));
                    lblP2WinPercentage.textProperty().bind(Bindings.concat(newValue.player2WinPercentage.asString("%.2f"), "%"));
                }
            }

        });

        try {
            List<Agent> agents = AgentInitialiser.getAllAgents();
            cmbAgent1.getItems().addAll(agents);
            cmbAgent2.getItems().addAll(agents);
        } catch (IOException ex) {
            Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void loadReplay(ActionEvent event) {
        if (chooser == null) {
            chooser = new DirectoryChooser();
        }
        File directory = chooser.showDialog(null);
        if (directory == null) {
            return;
        }
            if (directory.isDirectory()){
                chooser.setInitialDirectory(directory);
            }
        
        loadReplay(directory, true);
    }

    private void loadReplay(File directory, boolean autoSelect) {
        runLater(() -> {
            btnLoadReplay.setDisable(true);
            cmbxCurrentReplay.setDisable(true);
            btnBatchLoad.setDisable(true);
            //btnLiveMonitor.setDisable(true);
            GameStateLoader loader = new GameStateLoader();
            Task<SpaceInvaderMatch> t = loader.loadGameStates(directory);

            progressLoad.progressProperty().bind(t.progressProperty());
            progressLoad.setVisible(true);

            Thread thread = new Thread(t);
            thread.setDaemon(true);
            thread.start();

            Thread thread2 = new Thread(() -> {
                try {
                    SpaceInvaderMatch match = t.get();
                    if (match != null) {
                        addToMatchups(match);
                        Platform.runLater(() -> {
                            cmbxCurrentReplay.getItems().add(match);
                            if (autoSelect) {
                                cmbxCurrentReplay.setValue(match);
                            }
                        });
                    }
                } catch (InterruptedException ex) {
                } catch (ExecutionException ex) {
                    Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                }
                Platform.runLater(() -> {
                    progressLoad.progressProperty().unbind();
                    progressLoad.setVisible(false);
                    btnLoadReplay.setDisable(false);
                    cmbxCurrentReplay.setDisable(false);
                    btnBatchLoad.setDisable(false);
                    //btnLiveMonitor.setDisable(false);
                });
            });
            thread2.setDaemon(true);
            thread2.start();
        });
    }

    @FXML
    private void startTournament(ActionEvent event) {
        textRounds.setDisable(true);
        checkBoxPlaySelf.setDisable(true);
        btnRoundRobin.setDisable(true);
        btnCancelTournament.setVisible(true);
        btnCancelTournament.setDisable(false);
        Task<Void> tournamentTask = getTournamentTask();
        progressRoundRobin.progressProperty().bind(tournamentTask.progressProperty());
        lblRoundRobinProgress.textProperty().bind(tournamentTask.messageProperty());
        btnCancelTournament.setOnAction((e) -> {
            tournamentTask.cancel(true);
        });
        Thread t = new Thread(tournamentTask);
        t.setDaemon(true);
        t.start();

        Thread thread2 = new Thread(() -> {
            try {
                tournamentTask.get();
            } catch (InterruptedException | CancellationException ex) {
                //Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
            }
            Platform.runLater(() -> {
                textRounds.setDisable(false);
                checkBoxPlaySelf.setDisable(false);
                btnRoundRobin.setDisable(false);
                btnCancelTournament.setVisible(false);
                btnCancelTournament.setDisable(true);
                progressRoundRobin.progressProperty().unbind();
                progressRoundRobin.progressProperty().setValue(0);
                lblRoundRobinProgress.textProperty().unbind();
                lblRoundRobinProgress.textProperty().setValue("");
            });
        });
        thread2.setDaemon(true);
        thread2.start();
    }

    @FXML
    private void monitorFolder(ActionEvent event) {
        if (chooser == null) {
            chooser = new DirectoryChooser();
        }

        File replayDirectory = chooser.showDialog(null);
        if (replayDirectory != null) {
            if (replayDirectory.isDirectory()) {
                chooser.setInitialDirectory(replayDirectory);
            }
        }

        GameStateLoader loader = new GameStateLoader();

        Thread t = new Thread(() -> {
            try {
                loader.monitorFolderForGameStates(browser, replayDirectory.getCanonicalPath());
            } catch (IOException ex) {
                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        t.start();
    }

    @FXML
    private void createTestCases(ActionEvent event) {
        DirectoryChooser chooser = new DirectoryChooser();
        File directory = chooser.showDialog(null);
        if (directory != null) {
            if (directory.isDirectory()) {
                chooser.setInitialDirectory(directory);
            }
        }
        GameStateLoader loader = new GameStateLoader();
        btnTestCases.setDisable(true);
        Task<SpaceInvaderMatch> t = loader.loadGameStates(directory);
        progressTestCases.progressProperty().bind(t.progressProperty());
        progressTestCases.setVisible(true);
        Thread thread = new Thread(t);
        thread.start();

        Thread thread2 = new Thread(() -> {
            try {
                SpaceInvaderMatch match = t.get();
                StringBuilder builder = new StringBuilder();
                List<GameState> states = match.gameStates;
                Iterator<GameState> it = states.iterator();
                if (it.hasNext()) {
                    GameState curr = it.next();
                    while (it.hasNext()) {
                        GameState next = it.next();
                        if (next.player1ShotEnergy == curr.player1ShotEnergy + 1) {
                            if (next.player2ShotEnergy == curr.player2ShotEnergy + 1) {
                                //no bullets were fired by the aliens, good for test case
                                String testCase = getTestCase(curr, next);
                                builder.append(System.lineSeparator());
                                builder.append(testCase);
                            }
                        }
                        curr = next;
                    }
                    System.out.print(builder.toString());
                    final ClipboardContent content = new ClipboardContent();
                    content.putString(builder.toString());
                    Platform.runLater(() -> {
                        Clipboard.getSystemClipboard().setContent(content);
                    });

                }
            } catch (InterruptedException ex) {
            } catch (ExecutionException ex) {
                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
            }
            Platform.runLater(() -> {
                btnTestCases.setDisable(false);
                progressTestCases.progressProperty().unbind();
                progressTestCases.setVisible(false);
            });

        });
        thread2.start();
    }

    private String getTestCase(GameState curr, GameState next) {
        StringBuilder builder = new StringBuilder();
        builder.append("@Test").append(System.lineSeparator());
        builder.append("public void autogeneratedTest").append(System.currentTimeMillis()).append("").append(curr.round).append("() throws Exception {").append(System.lineSeparator());
        builder.append("String state = \"\"").append(System.lineSeparator());
        String split[] = curr.stateStringProperty().get().split("\n");
        for (int i = 0; i < split.length; i++) {
            builder.append("+ \"").append(split[i].trim()).append("\\n\"").append(System.lineSeparator());
        }
        builder.append(";").append(System.lineSeparator());

        builder.append("String result = \"\"").append(System.lineSeparator());
        split = next.stateStringProperty().get().split("\n");
        for (int i = 0; i < split.length; i++) {
            builder.append("+ \"").append(split[i].trim()).append("\\n\"").append(System.lineSeparator());
        }
        builder.append(";").append(System.lineSeparator());

        String p1Move = getMove(next.player1LastMoveProperty().get());
        String p2Move = getMove(next.player2LastMoveProperty().get());

        builder.append("assertTrue(\"Auto generated test failed.\", MoveSimulator.expectedResult(state, ").append(p1Move).append(", ").append(p2Move).append(", result));").append(System.lineSeparator());
        builder.append("}").append(System.lineSeparator());
        return builder.toString();
    }

    public void addToMatchups(SpaceInvaderMatch match) {
        if (match.gameStates.isEmpty()) {
            Logger.getLogger(MainScreenController.class.getName()).fine("Warning, empty match.");
            return;
        }
        synchronized (matchupLock) {
            String matchIdentifier;
            if (match.player1Name.compareTo(match.player2Name) < 1) {
                matchIdentifier = match.player1Name + "-" + match.player2Name;
            } else {
                matchIdentifier = match.player1Name + "-" + match.player2Name;
            }
            if (matchups.containsKey(matchIdentifier)) {
                Matchup m = matchups.get(matchIdentifier);
                m.addMatch(match);
            } else {
                Matchup m = new Matchup(match.player1Name, match.player2Name, match);
                matchups.put(matchIdentifier, m);
                runLater(() -> lstMatchup.getItems().add(m));
            }
        }
    }

    public void runLater(Runnable r) {
        if (Platform.isFxApplicationThread()) {
            r.run();
        } else {
            Platform.runLater(r);
        }
    }

    private Task<Void> getTournamentTask() {
        final boolean playSelf = checkBoxPlaySelf.isSelected();
        int rounds;
        try {
            rounds = Integer.parseInt(textRounds.getText());
        } catch (NumberFormatException e) {
            rounds = 10;
        }
        final int numExperiments = rounds;
        Task<Void> tournamentTask = new Task<Void>() {
            TestHarness current;
            boolean cancelled = false;

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                cancelled = true;
                if (current != null) {
                    current.cancel();
                }
                return super.cancel(mayInterruptIfRunning);
            }

            @Override
            protected Void call() throws Exception {
                File f = new File("./replays");
                f.mkdirs();

                try {
                    List<TestableAgent> agents = AgentInitialiser.getAllTestableAgents();
                    List<TestableAgent> agents2 = AgentInitialiser.getAllTestableAgents();
                    int count = 0;
                    int total = 0;
                    int jOffset = 1;
                    if (playSelf) {
                        jOffset = 0;
                        total = agents.size() + (agents.size() * (agents.size() - 1)) / 2;
                    } else {
                        total = (agents.size() * (agents.size() - 1)) / 2;
                    }

                    total = total * numExperiments;

                    for (int i = 0; i < agents.size(); i++) {
                        for (int j = i + jOffset; j < agents.size(); j++) {
                            for (int k = 0; k < numExperiments; k++) {
                                if (cancelled) {
                                    return null;
                                }
                                this.updateProgress(count, total);
                                count++;

                                TestHarness t = new TestHarness(agents.get(i), agents2.get(j));
                                current = t;
                                File outfolder = getNextReplayFolder(f);
                                SimpleStringProperty progress = new SimpleStringProperty();
                                progress.addListener(new ChangeListener<String>() {
                                    @Override
                                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                        updateMessage(newValue);
                                    }
                                });
                                t.simulateGame(outfolder, 250, true, true, progress);
                                loadReplay(outfolder, false);
                            }
                        }
                    }
                    this.updateProgress(count, total);
                } catch (IOException ex) {
                    if (cancelled) {
                        return null;
                    }
                    //Logger.getLogger(DeveloperPane.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }

        };
        return tournamentTask;
    }

    public Task<Void> battleAgents(TestableAgent t1, TestableAgent t2, int times) {
        File f = new File("./replays");
        f.mkdirs();
        Task<Void> tournamentTask = new Task<Void>() {
            TestHarness current;
            boolean cancelled = false;

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                cancelled = true;
                if (current != null) {
                    current.cancel();
                }
                return super.cancel(mayInterruptIfRunning);
            }

            @Override
            protected Void call() throws Exception {
                File f = new File("./replays");
                f.mkdirs();

                int count = 0;
                int total = 0;
                total = times;
                for (int k = 0; k < times; k++) {
                    if (cancelled) {
                        return null;
                    }

                    this.updateProgress(count, total);
                    count++;
                    TestHarness t = new TestHarness(t1, t2);
                    current = t;
                    File outfolder = getNextReplayFolder(f);
                    SimpleStringProperty progress = new SimpleStringProperty();
                    progress.addListener(new ChangeListener<String>() {
                        @Override
                        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                            updateMessage(newValue);
                        }
                    });
                    t.simulateGame(outfolder, 250, true, true, progress);
                    loadReplay(outfolder, false);
                }
                this.updateProgress(count, total);
                return null;
            }
        };
        return tournamentTask;
    }

    public File getNextReplayFolder(File parentFolder) {
        File files[] = parentFolder.listFiles();
        int largest = 0;
        for (File file : files) {
            if (file.isDirectory()) {
                try {
                    largest = Math.max(largest, Integer.parseInt(file.getName()));
                } catch (NumberFormatException e) {

                }
            }
        }
        largest++;
        File outfolder = new File(parentFolder.getAbsolutePath() + "/" + String.format("%04d", largest));
        return outfolder;
    }

    public String getMove(String move) {
        if (move.equals("Nothing")) {
            return "Move.NONE";
        } else if (move.equals("MoveLeft")) {
            return "Move.LEFT";
        } else if (move.equals("MoveRight")) {
            return "Move.RIGHT";
        } else if (move.equals("Shoot")) {
            return "Move.FIRE";
        } else if (move.equals("BuildShield")) {
            return "Move.BUILD_SHIELDS";
        } else if (move.equals("BuildMissileController")) {
            return "Move.BUILD_MISSILE_FACTORY";
        } else if (move.equals("BuildAlienFactory")) {
            return "Move.BUILD_ALIEN_FACTORY";
        } else {
            return "Move.UNKNOWN";
        }
    }

    @FXML
    private void loadBatch(ActionEvent event) {
if (chooser == null){
        chooser = new DirectoryChooser();
}
        File directory = chooser.showDialog(null);
        if (directory == null) {
            return;
        }

        if (directory.isDirectory()) {
            chooser.setInitialDirectory(directory);
        }

        btnLoadReplay.setDisable(true);
        cmbxCurrentReplay.setDisable(true);
        btnBatchLoad.setDisable(true);
        //btnLiveMonitor.setDisable(true);
        GameStateLoader loader = new GameStateLoader();
        Task<List<SpaceInvaderMatch>> t = loader.loadMatches(directory);
        progressLoad.progressProperty().bind(t.progressProperty());
        progressLoad.setVisible(true);
        Thread thread = new Thread(t);
        thread.setDaemon(true);
        thread.start();

        Thread thread2 = new Thread(() -> {
            try {
                List<SpaceInvaderMatch> matches = t.get();
                if (matches != null) {
                    for (SpaceInvaderMatch m : matches) {
                        addToMatchups(m);
                        Platform.runLater(() -> {
                            cmbxCurrentReplay.getItems().add(m);
                            cmbxCurrentReplay.setValue(m);
                        });
                    }
                }
            } catch (InterruptedException ex) {

            } catch (ExecutionException ex) {
                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
            }
            Platform.runLater(() -> {
                progressLoad.progressProperty().unbind();
                progressLoad.setVisible(false);
                btnLoadReplay.setDisable(false);
                cmbxCurrentReplay.setDisable(false);
                btnBatchLoad.setDisable(false);
                //btnLiveMonitor.setDisable(false);
            });
        });
        thread2.setDaemon(true);
        thread2.start();
    }

    @FXML
    private void battleAgents(ActionEvent event) {
        Agent agent1 = cmbAgent1.getValue();
        Agent agent2 = cmbAgent2.getValue();
        if (agent1 == null || agent2 == null) {
            return;
        }
        if (agent1.getDefaultInstance() == null || agent2.getDefaultInstance() == null) {
            return;
        }
        int rounds;
        try {
            rounds = Integer.parseInt(textRoundsBattle.getText());
        } catch (NumberFormatException e) {
            rounds = 1;
        }

        textRoundsBattle.setDisable(true);
        //checkBoxWatch.setDisable(true);
        btnBattle.setDisable(true);
        btnCancelBattle.setVisible(true);
        btnCancelBattle.setDisable(false);
        Task<Void> battleAgents = battleAgents(agent1.getNewInstance(), agent2.getNewInstance(), rounds);
        progressBattle.progressProperty().bind(battleAgents.progressProperty());
        lblBattleProgress.textProperty().bind(battleAgents.messageProperty());
        btnCancelBattle.setOnAction((e) -> {
            battleAgents.cancel(true);
        });
        Thread t = new Thread(battleAgents);
        t.setDaemon(true);
        t.start();

        Thread thread2 = new Thread(() -> {
            try {
                battleAgents.get();
            } catch (InterruptedException | CancellationException ex) {

            } catch (ExecutionException ex) {
                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
            }
            Platform.runLater(() -> {
                textRoundsBattle.setDisable(false);
                //checkBoxWatch.setDisable(false);
                btnBattle.setDisable(false);
                btnCancelBattle.setVisible(false);
                btnCancelBattle.setDisable(true);
                progressBattle.progressProperty().unbind();
                progressBattle.progressProperty().setValue(0);
                lblBattleProgress.textProperty().unbind();
                lblBattleProgress.textProperty().setValue("");
            });
        });
        thread2.setDaemon(true);
        thread2.start();
    }

}
