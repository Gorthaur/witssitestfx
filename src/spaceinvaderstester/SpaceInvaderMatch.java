package spaceinvaderstester;

import java.util.LinkedList;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

/**
 *
 * @author Dean
 */
public class SpaceInvaderMatch {

    StringProperty matchNameProperty = new SimpleStringProperty();
    String player1Name;
    String player2Name;
    ObservableList<GameState> gameStates = FXCollections.observableList(new LinkedList<GameState>());
    XYChart.Series player1Kills = new XYChart.Series();
    XYChart.Series player2Kills = new XYChart.Series();
    XYChart.Series player1Lives = new XYChart.Series();
    XYChart.Series player2Lives = new XYChart.Series();
    boolean player1Winner;
    boolean player2Winner;

    boolean stillLoading = true;

    public SpaceInvaderMatch(String matchId, List<GameState> gameStates) {
        matchNameProperty.setValue(matchId);
        if (gameStates == null || gameStates.isEmpty()) {
        } else {
            initialiseMatch(gameStates.get(0));
            for (GameState g : gameStates) {
                addState(g);
            }
            stillLoading = false;
        }
    }

    private void initialiseMatch(GameState g) {
        matchNameProperty.setValue(matchNameProperty.get() + " - " + g.player1Name + " vs " + g.player2Name);
        player1Name = g.player1Name;
        player2Name = g.player2Name;
        player1Kills.setName(g.player1Name + " kills");
        player2Kills.setName(g.player2Name + " kills");
        player1Lives.setName(g.player1Name + " lives");
        player2Lives.setName(g.player2Name + " lives");
    }

    private void addState(GameState g) {
        if (player1Name == null) {
            initialiseMatch(g);
        }
        gameStates.add(g);
        player1Kills.getData().add(new XYChart.Data(g.round, g.player1Kills));
        player1Lives.getData().add(new XYChart.Data(g.round, g.player1Lives));
        player2Kills.getData().add(new XYChart.Data(g.round, g.player2Kills));
        player2Lives.getData().add(new XYChart.Data(g.round, g.player2Lives));
        determineWinner(g);
    }

    public void addGameState(GameState g) {
        if (Platform.isFxApplicationThread()) {
            addState(g);

        } else {
            Platform.runLater(() -> addGameState(g));
        }
    }

    private void determineWinner(GameState g) {
        if (!g.player1ShipAlive && g.player1Lives == 0) {
            if (!g.player2ShipAlive && g.player2Lives == 0) {
                if (g.player1Kills > g.player2Kills) {
                    player1Winner = true;
                    player2Winner = false;
                } else if (g.player2Kills > g.player1Kills) {
                    player2Winner = true;
                    player1Winner = false;
                } else { //draw
                    player1Winner = true;
                    player2Winner = false;
                }
            } else {
                player1Winner = false;
                player2Winner = true;
            }
        } else if (!g.player2ShipAlive && g.player2Lives == 0) {
            player1Winner = true;
            player2Winner = false;
        } else if (g.round >= 200) {
            if (g.player1Kills > g.player2Kills) {
                player1Winner = true;
                player2Winner = false;
            } else if (g.player1Kills < g.player2Kills) {
                player1Winner = false;
                player2Winner = true;
            } else { //draw
                player1Winner = true;
                player2Winner = false;
            }
        } else {
            //no winner yet, or after round 200 for extra long games
            player1Winner = false;
            player2Winner = false;
        }
    }

    @Override
    public String toString() {
        return matchNameProperty.get();
    }

    public void completeLoading() {
        stillLoading = false;
    }

}
