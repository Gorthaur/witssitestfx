package spaceinvaderstester;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import spaceinvaderstester.harness.TestHarness;
import utilities.AgentInitialiser;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 *
 * @author Dean
 */
public class DeveloperPane extends StackPane {

    HBox hboxRoundRobin = new HBox();
    TextField roundRobinNum = new TextField();
    Button roundRobin = new Button("Round Robin");

    public DeveloperPane() {
        hboxRoundRobin.getChildren().addAll(roundRobinNum, roundRobin);
        this.getChildren().add(hboxRoundRobin);
        roundRobin.setOnAction((e) -> {
            runTournament();
        });

    }

    private void runTournament() {


    }
}
