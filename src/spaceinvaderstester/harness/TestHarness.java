package spaceinvaderstester.harness;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.StringProperty;
import javafx.geometry.Point2D;
import spaceinvaderstester.GameState;
import utilities.MoveSimulator;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;
import za.ac.wits.witstestablespaceinvaders.TestableGameState;

/**
 *
 * @author Dean
 */
public class TestHarness {

    private final TestableAgent player1;
    private final TestableAgent player2;
    private final Object cancelLock = new Object();
    private boolean cancelled = false;

    public TestHarness(TestableAgent player1, TestableAgent player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void cancel() {
        synchronized (cancelLock) {
            this.cancelled = true;
        }
    }

    public boolean isCancelled() {
        synchronized (cancelLock) {
            if (cancelled) {
                cancelled = false;
                return true;
            }
            return false;
        }
    }

    public void simulateGame(File replayFolder, int maxRounds, boolean stopWhenP1Dead, boolean stopWhenP2Dead, StringProperty statusProperty) throws IOException {
        replayFolder.mkdirs();
        String initialState = "###################\n"
                + String.format("# %-15s #\n", player2.getDescription())
                + "# Wave Size:  3   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 0/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   0      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 1000     #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#       VVV       #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#       AAA       #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 0/1   #\n"
                + "# Lives: 2        #\n"
                + "# Kills: 0        #\n"
                + "# Round:   0      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 0/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + String.format("# %-15s #\n", player1.getDescription())
                + "###################\n";

        String currentStateString = initialState;
        ByteArrayInputStream in1;
        ByteArrayInputStream in2;
        Move player1Move = Move.NONE;
        Move player2Move = Move.NONE;
        ExecutorService moveThreads = Executors.newFixedThreadPool(2);
        for (int i = 0; i < maxRounds; i++) {
            if (isCancelled()) {
                return;
            }
            try {
                statusProperty.setValue(player1.getDescription() + " vs " + player2.getDescription() + " - round " + i);
                File f = new File(replayFolder.getAbsoluteFile() + "/" + String.format("%03d", i));
                f.mkdirs();
                File outfile = new File(f.getAbsoluteFile() + "/map-detailed.txt");
                FileWriter fw = new FileWriter(outfile);
                fw.write(currentStateString.replaceAll("[\n\r]+", System.lineSeparator()));
                fw.close();
                File move1 = new File(f.getAbsoluteFile() + "/move1.txt");
                fw = new FileWriter(move1);
                fw.write(player1.getDescription() + ": " + player1Move + "              > Move result.");
                fw.close();
                File move2 = new File(f.getAbsoluteFile() + "/move2.txt");
                fw = new FileWriter(move2);
                fw.write(player2.getDescription() + ": " + player2Move + "              > Move result.");
                fw.close();
                in1 = new ByteArrayInputStream(currentStateString.getBytes());
                in2 = new ByteArrayInputStream(MoveSimulator.flipBoard(currentStateString).getBytes());
                TestableGameState g1 = player1.getGameState(in1);
                boolean p1Dead = false;
                if (g1.player1ShipAlive() == false && g1.getPlayer1Lives() == 0) {
                    p1Dead = true;
                }
                boolean p2Dead = false;
                if (g1.player2ShipAlive() == false && g1.getPlayer2Lives() == 0) {
                    p2Dead = true;
                }
                if ((p1Dead == true && p2Dead == true) || (p1Dead && stopWhenP1Dead) || (p2Dead && stopWhenP2Dead)) {
                    break;
                }
                final TestableAgent p1 = player1;
                final TestableAgent p2 = player2;
                final InputStream imp1 = new ByteArrayInputStream(currentStateString.getBytes());;
                final InputStream imp2 = in2;
                FutureTask<Move> ft1 = new FutureTask(() -> {
                    return p1.getMove(imp1);
                });
                FutureTask<Move> ft2 = new FutureTask(() -> {
                    return p2.getMove(imp2);
                });
                moveThreads.submit(ft1);
                moveThreads.submit(ft2);
                player1Move = ft1.get();
                player2Move = ft2.get();
                LinkedList<Point2D> bullets = new LinkedList<>();
                if (g1.getPlayer1AlienPrimaryBullet() != null) {
                    if (Math.random() <= 1 / 3.0) {
                        bullets.add(g1.getPlayer1AlienPrimaryBullet());
                    } else {
                        if (g1.getPlayer1AlienSecondaryBullets().size() > 0) {
                            int num = (int) (Math.random() * g1.getPlayer1AlienSecondaryBullets().size());
                            if (num == g1.getPlayer1AlienSecondaryBullets().size()) {
                                num--;
                            }
                            bullets.add(g1.getPlayer1AlienSecondaryBullets().get(num));
                        }
                    }
                }
                if (g1.getPlayer2AlienPrimaryBullet() != null) {
                    if (Math.random() <= 1 / 3.0) {
                        bullets.add(g1.getPlayer2AlienPrimaryBullet());
                    } else {
                        if (g1.getPlayer2AlienSecondaryBullets().size() > 0) {
                            int num = (int) (Math.random() * g1.getPlayer2AlienSecondaryBullets().size());
                            if (num == g1.getPlayer2AlienSecondaryBullets().size()) {
                                num--;
                            }
                            bullets.add(g1.getPlayer2AlienSecondaryBullets().get(num));
                        }
                    }
                }
                Point2D bulletArr[] = new Point2D[bullets.size()];
                g1 = g1.getNextState(player1Move, player2Move, bullets.toArray(bulletArr));
                currentStateString = g1.getMapString();
            } catch (InterruptedException | ExecutionException ex) {
                if (isCancelled()) {
                    return;
                }
                Logger.getLogger(TestHarness.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
