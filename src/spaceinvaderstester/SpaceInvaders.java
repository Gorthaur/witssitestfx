package spaceinvaderstester;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Dean
 */
public class SpaceInvaders extends Application {

    @Override
    public void start(Stage primaryStage) {
        StackPane root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("MainScreen.fxml"));

            root = (StackPane) loader.load();
            
            Scene scene = new Scene(root);
            root.getStylesheets().add(SpaceInvaders.class.getResource("spaceinvadersgui.css").toExternalForm());

            primaryStage.setTitle("Space Invaders");
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setOnCloseRequest((e)->{
                System.exit(0);
            });
        } catch (IOException ex) {
            Logger.getLogger(SpaceInvaders.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
