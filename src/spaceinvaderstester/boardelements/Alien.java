package spaceinvaderstester.boardelements;

import javafx.animation.Animation;
import static javafx.animation.Animation.INDEFINITE;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 *
 * @author Dean
 */
public class Alien extends BoardElement {

    private int direction;

    public Alien(int positionX, int positionY) {
        super(1, 1, positionX, positionY);
        if (positionY <= 11) {
            this.setRotate(0);
        }
        mainImage.getStyleClass().add("alien");
        secondaryImage.setTranslateX(-scale.get() / 2);
        secondaryImage.setTranslateY(-scale.get() / 2);
    }

    public Alien(int positionX, int positionY, int direction) {
        super(1, 1, positionX, positionY);
        this.setRotate(0);
        this.direction = direction;
        mainImage.getStyleClass().add("alien");
        secondaryImage.setTranslateX(-scale.get() / 2);
        secondaryImage.setTranslateY(-scale.get() / 2);
    }

    @Override
    protected Animation getMovementAnimation(final long duration) {
        final long actualCycleTime = 300;
        SpriteAnimation anim = new SpriteAnimation(mainImage, getMovingSprite(this.scale.get()), this.scale.get(), duration, actualCycleTime);
        return anim;
    }

    @Override
    protected Animation getCreationAnimation(long duration) {
        this.setScaleX(0.1);
        this.setScaleY(0.1);
        SpriteAnimation anim = new SpriteAnimation(mainImage, getCreationSprite(this.scale.get()), this.scale.get(), duration);

        ScaleTransition scale = new ScaleTransition(Duration.millis(duration), this);
        scale.setFromX(0.4);
        scale.setToX(1);
        scale.setFromY(0.4);
        scale.setToY(1);

        ParallelTransition both = new ParallelTransition(anim, scale);
        return both;
    }

    @Override
    protected Animation getIdleAnimation() {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getIdleSprite(this.scale.get()), scale.get(), 750, 750, true);
        anim.setAutoReverse(true);
        return anim;
    }

    @Override
    public Image getMovingSprite(double scale) {
        if (this.direction == -1) {
            return BoardElement.loadImage("alienTopMove.png", scale);
        } else {
            return BoardElement.loadImage("alienBotMove.png", scale);
        }
    }

    @Override
    public Image getIdleSprite(double scale) {
        if (this.direction == -1) {

            return BoardElement.loadImage("alienTopIdle.png", scale);

        } else {

            return BoardElement.loadImage("alienBotIdle.png", scale);

        }
    }

    @Override
    public Image getCreationSprite(double scale) {
        if (this.direction == -1) {
            return BoardElement.loadImage("alienTopEmerge.png", scale);
        } else {
            return BoardElement.loadImage("alienBotEmerge.png", scale);
        }
    }

    @Override
    public Image getDestructionSprite(double scale) {
        return BoardElement.loadImage("greenExplosion2.png", scale);
    }

    @Override
    protected Animation getDestructionAnimation(long duration) {
        SpriteAnimation anim = new SpriteAnimation(secondaryImage, getDestructionSprite(this.scale.get() * 2), this.scale.get() * 2, duration);
        return anim;
    }

}
