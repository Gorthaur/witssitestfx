package spaceinvaderstester.boardelements;

import java.util.concurrent.ConcurrentHashMap;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 *
 * @author Dean
 */
public abstract class BoardElement extends Group {

    private final int width;
    private final int height;
    private int positionX;
    private int positionY;
    final protected ImageView mainImage;
    final protected ImageView secondaryImage;
    Animation idleAnimation;

    static ConcurrentHashMap<String, ConcurrentHashMap<Double, Image>> imageStore = new ConcurrentHashMap<>();

    SimpleDoubleProperty scale = new SimpleDoubleProperty(25);

    final DoubleProperty scaleProperty() {
        return scale;
    }
    Image movingSprite;
    Image idleSprite;
    Image creationSprite;
    Image destructionSprite;

    public BoardElement(int width, int height, int positionX, int positionY) {
        this.width = width;
        this.height = height;
        this.positionX = positionX;
        this.positionY = positionY;
        //if (positionY <= 11) {
        //    this.setRotate(180);
        //}

        this.setTranslateX((positionX - (int) (width / 2)) * scaleProperty().get());
        this.setTranslateY((positionY - (int) (height / 2)) * scaleProperty().get());

        mainImage = new ImageView();
        secondaryImage = new ImageView();
        this.getChildren().addAll(mainImage, secondaryImage);

        this.parentProperty().addListener(new ChangeListener<Parent>() {

            @Override
            public void changed(ObservableValue<? extends Parent> observable, Parent oldValue, Parent newValue) {
                if (newValue == null) {
                    if (idleAnimation != null) {
                        idleAnimation.stop();
                    }
                }
            }

        });
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void startAnimation() {
        startIdleAnimation();
    }

    public abstract Image getMovingSprite(double scale);

    public abstract Image getIdleSprite(double scale);

    public abstract Image getCreationSprite(double scale);

    public abstract Image getDestructionSprite(double scale);

    protected Image getCrtSprite(double scale) {
        if (creationSprite == null || creationSprite.getHeight() / height != scale) {
            creationSprite = getCreationSprite(scale);
        }

        return creationSprite;
    }

    protected Image getDesSprite(double scale) {
        if (destructionSprite == null || destructionSprite.getHeight() / height != scale) {
            destructionSprite = getDestructionSprite(scale);
        }

        return destructionSprite;
    }

    protected Image getMvSprite(double scale) {
        if (movingSprite == null || movingSprite.getHeight() / height != scale) {
            movingSprite = getMovingSprite(scale);
        }

        return movingSprite;
    }

    protected Image getIdSprite(double scale) {
        if (idleSprite == null || idleSprite.getHeight() / height != scale) {
            idleSprite = getIdleSprite(scale);
        }

        return idleSprite;
    }

    public final Animation getMovementAnimation(int x, int y, long duration) {
        //mainImage.setImage(getMvSprite(scaleProperty().get()));
        Animation translate;
        //if (x == this.positionX && y == this.positionY) {
        //    translate = getTranslationAnimation(0, 0, 0, 0, duration);
        //} else {
        translate = getTranslationAnimation((this.positionX - (int) (width / 2)) * scale.get(), (this.positionY - (int) (height / 2)) * scale.get(), (x - (int) (width / 2)) * scale.get(), (y - (int) (height / 2)) * scale.get(), duration);
        //}
        Animation anim = getMovementAnimation(duration);
        this.positionX = x;
        this.positionY = y;
        ParallelTransition t;
        if (anim == null) {
            t = new ParallelTransition(translate);
        } else {
            if (idleAnimation != null) {
                idleAnimation.stop();
            }
            t = new ParallelTransition(translate, anim);
        }

        //t.setOnFinished((e) -> {
        //    startIdleAnimation();
        //});
        return t;
    }

    public final Animation getCreationAnimation(int x, int y, long duration) {
        Animation translate;
        //if (x == this.positionX && y == this.positionY) {
        //    translate = getTranslationAnimation(0, 0, 0, 0, duration);
        //} else {
        translate = getTranslationAnimation((this.positionX - width / 2) * scale.get(), (this.positionY - height / 2) * scale.get(), (x - width / 2) * scale.get(), (y - height / 2) * scale.get(), duration);
        //}
        Animation anim = getCreationAnimation(duration);

        this.positionX = x;
        this.positionY = y;
        ParallelTransition t;
        if (anim == null) {
            t = new ParallelTransition(translate);
        } else {
            if (idleAnimation != null) {
                idleAnimation.stop();
            }

            t = new ParallelTransition(translate, anim);
        }

        //t.setOnFinished((e) -> {
        //    startIdleAnimation();
        //});
        return t;
    }

    public void startIdleAnimation() {
        if (idleAnimation == null) {
            idleAnimation = getIdleAnimation();
        }
        if (!idleAnimation.getStatus().equals(Animation.Status.RUNNING)) {
            idleAnimation.playFromStart();
        }
    }
    
    public void stopIdleAnimation() {
        if (!Platform.isFxApplicationThread()) {
            Platform.runLater(()->stopIdleAnimation());
        }
        else {
            if (idleAnimation != null) {
                idleAnimation.stop();
            }
        }
    }

    protected final Animation getTranslationAnimation(final double fromX, final double fromY, final double toX, final double toY, long duration) {
        TranslateTransition t = new TranslateTransition(Duration.millis(duration), this);
        t.setFromX(fromX);
        t.setFromY(fromY);
        t.setToX(toX);
        t.setToY(toY);
        return t;
    }

    public final Animation getDestructionAnimation(int deltaX, int deltaY, long duration) {
        ParallelTransition parallel = new ParallelTransition();

        Animation move = getMovementAnimation(positionX + deltaX, positionY + deltaY, duration);
        parallel.getChildren().add(move);

        FadeTransition fade = new FadeTransition(Duration.millis(duration), mainImage);
        fade.setFromValue(1);
        fade.setToValue(0);
        parallel.getChildren().add(fade);
        Animation destruction = getDestructionAnimation(duration);
        if (destruction != null) {
            parallel.getChildren().add(destruction);
        }
        return parallel;
    }

    protected abstract Animation getMovementAnimation(long duration);

    protected Animation getDestructionAnimation(long duration) {
        return null;
    }

    protected Animation getCreationAnimation(long duration) {
        return null;
    }

    protected abstract Animation getIdleAnimation();

    public int getSpriteWidth() {
        return width;
    }

    public int getSpriteHeight() {
        return height;
    }

    static Image loadImage(String fileName, double scale) {
        if (imageStore.containsKey(fileName)) {
            if (imageStore.get(fileName).containsKey(scale)) {
                return imageStore.get(fileName).get(scale);
            } else {
                imageStore.get(fileName).put(scale, new Image(BoardElement.class.getResourceAsStream(fileName), Double.MAX_VALUE, scale, true, true));
            }
        } else {
            imageStore.put(fileName, new ConcurrentHashMap<>());
        }
        return loadImage(fileName, scale);
    }
}
