package spaceinvaderstester.boardelements;

import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 *
 * @author Dean
 */
public class SpriteAnimation extends Transition {

    private final long duration;
    private final ImageView imageView;
    private final Image image;
    private final double spriteWidth;
    private final long fixedCycleDuration;
    private Rectangle2D port = null;
    int frame = -1;

    public SpriteAnimation(ImageView imageView, Image image, double spriteWidth, long duration) {
        this(imageView, image, spriteWidth, duration, duration);
    }

    public SpriteAnimation(ImageView imageView, Image image, double spriteWidth, long duration, long fixedCycleDuration) {
        this(imageView, image, spriteWidth, duration, fixedCycleDuration, false);
    }

    public SpriteAnimation(ImageView imageView, Image image, double spriteWidth, long duration, long fixedCycleDuration, boolean infinite) {
        super(40);
        this.imageView = imageView;
        this.image = image;
        this.spriteWidth = spriteWidth;
        this.duration = duration;
        this.fixedCycleDuration = fixedCycleDuration;
        this.setCycleDuration(Duration.millis(duration));
        if (infinite) {
            this.setCycleCount(Animation.INDEFINITE);
        } else {
            this.setCycleCount(1);
        }
    }

    @Override
    public void play() {
        runLater(() -> {
            double millis = this.getCurrentTime().toMillis() % fixedCycleDuration;
            double frac = millis / fixedCycleDuration;
            interp(frac);
        });

        super.play();
    }

    private void interp(double frac) {
        if (imageView.getImage() == null || !imageView.getImage().equals(image)) {
            imageView.setImage(image);
        }
        double width = image.getWidth();
        double unitHeight = image.getHeight();
        int tempFrame = Math.min((int) (frac * (width / spriteWidth)), (int) ((width / spriteWidth) - 1));
        if (frame == tempFrame) {
            if (imageView.getViewport() != port) {
                imageView.setViewport(port);
            }
        } else {
            frame = tempFrame;
            port = new Rectangle2D(frame * spriteWidth, 0, spriteWidth, unitHeight);
            imageView.setViewport(port);
        }
    }

    @Override
    protected void interpolate(double frac) {
        interp(((frac * duration) % fixedCycleDuration) / (double) fixedCycleDuration);
    }

    public void runLater(Runnable r) {
        if (Platform.isFxApplicationThread()) {
            r.run();
        } else {
            Platform.runLater(r);
        }
    }

}
