/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaderstester.boardelements;

import javafx.animation.Animation;
import static javafx.animation.Animation.INDEFINITE;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.util.Duration;

/**
 *
 * @author Dean
 */
public class Bullet extends BoardElement {

    public Bullet(int positionX, int positionY) {
        super(1, 1, positionX, positionY);
        secondaryImage.setTranslateY(scale.get() * 0.2);
        secondaryImage.setTranslateX(-scale.get() / 4);
        secondaryImage.setTranslateY(-scale.get() / 4 + scale.get() * 0.2);
    }

    @Override
    protected Animation getMovementAnimation(final long duration) {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getMovingSprite(this.scale.get()), this.scale.get(), duration, 300);
        return anim;
    }

    @Override
    protected Animation getIdleAnimation() {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getIdleSprite(this.scale.get()), this.scale.get(), 600, 600, true);
        return anim;
    }

    @Override
    protected Animation getCreationAnimation(long duration) {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getCreationSprite(this.scale.get()), this.scale.get(), duration);
        return anim;
    }

    @Override
    protected Animation getDestructionAnimation(long duration) {
        Image destruction = getDestructionSprite(this.scale.get() * 1.5);
        SpriteAnimation anim = new SpriteAnimation(secondaryImage, destruction, destruction.getHeight(), duration);
        return anim;
    }

    @Override
    public Image getMovingSprite(double scale) {
        if (getPositionY() <= 11) {
            return BoardElement.loadImage("bulletTop.png", scale);
        } else {
            return BoardElement.loadImage("bullet.png", scale);
        }
    }

    @Override
    public Image getIdleSprite(double scale) {
        if (getPositionY() <= 11) {
            return BoardElement.loadImage("bulletTop.png", scale);
        } else {
            return BoardElement.loadImage("bullet.png", scale);
        }
    }

    @Override
    public Image getCreationSprite(double scale) {
        if (getPositionY() <= 11) {
            return BoardElement.loadImage("bulletfireTop.png", scale);
        } else {
            return BoardElement.loadImage("bulletfire.png", scale);
        }
    }

    @Override
    public Image getDestructionSprite(double scale) {
        return BoardElement.loadImage("redExplosion.png", scale);
    }

}
