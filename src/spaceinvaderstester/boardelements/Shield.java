package spaceinvaderstester.boardelements;

import javafx.animation.Animation;
import static javafx.animation.Animation.INDEFINITE;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.util.Duration;

/**
 *
 * @author Dean
 */
public class Shield extends BoardElement {

    public Shield(int positionX, int positionY) {
        super(1, 1, positionX, positionY);
        secondaryImage.setTranslateX(-scale.get() / 4);
        secondaryImage.setTranslateY(-scale.get() / 4);
        this.setRotate(0);
    }

    @Override
    protected Animation getMovementAnimation(long duration) {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getMovingSprite(this.scale.get()), this.scale.get(), duration);
        return anim;
    }
    
        @Override
    protected Animation getCreationAnimation(long duration) {
                SpriteAnimation anim = new SpriteAnimation(mainImage, getCreationSprite(this.scale.get()), this.scale.get(), duration);
        return anim;
    }

    @Override
    protected Animation getDestructionAnimation(long duration) {
        Image destruction = getDestructionSprite(this.scale.get() * 1.5);
        SpriteAnimation anim = new SpriteAnimation(secondaryImage, destruction, destruction.getHeight(), duration);
        return anim;
    }

    @Override
    protected Animation getIdleAnimation() {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getIdleSprite(this.scale.get()), this.scale.get(), 300, 300, true);
        return anim;
    }

    @Override
    public Image getMovingSprite(double scale) {
        return BoardElement.loadImage("fenceBot.png", scale);
    }

    @Override
    public Image getIdleSprite(double scale) {
        return BoardElement.loadImage("fenceBot.png", scale);
    }

    @Override
    public Image getCreationSprite(double scale) {
        return BoardElement.loadImage("fenceBot.png", scale);
    }

    @Override
    public Image getDestructionSprite(double scale) {
        return BoardElement.loadImage("blueExplosion.png", scale);
    }

}
