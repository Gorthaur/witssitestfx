/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaderstester.boardelements;

import javafx.animation.Animation;
import static javafx.animation.Animation.INDEFINITE;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.util.Duration;

/**
 *
 * @author Dean
 */
public class Ship extends BoardElement {

    private int side = 1;

    public Ship(int positionX, int positionY) {
        super(3, 1, positionX, positionY);
        secondaryImage.setTranslateY(-scale.get());
        setRotate(0);
        if (positionY <= 11) {
            this.side = -1;
        }
    }

    @Override
    protected Animation getMovementAnimation(final long duration) {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getMovingSprite(this.scale.get()), this.scale.get() * 3, duration, 300);
        return anim;
    }

    @Override
    protected Animation getIdleAnimation() {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getIdleSprite(this.scale.get()), this.scale.get() * 3, 300, 300, true);
        return anim;
    }

    @Override
    protected Animation getDestructionAnimation(long duration) {
        SpriteAnimation anim = new SpriteAnimation(secondaryImage, getDestructionSprite(this.scale.get()*3), this.scale.get() * 3, duration);
        return anim;
    }

    @Override
    public Image getMovingSprite(double scale) {
        if (this.side == -1) {
            return BoardElement.loadImage("shipTop.png", scale);
        }
        return BoardElement.loadImage("shipBot.png", scale);
    }

    @Override
    public Image getIdleSprite(double scale) {
        if (this.side == -1) {
            return BoardElement.loadImage("shipTop.png", scale);
        }
        return BoardElement.loadImage("shipBot.png", scale);
    }

    @Override
    public Image getCreationSprite(double scale) {
        if (this.side == -1) {
            return BoardElement.loadImage("shipTop.png", scale);
        }
        return BoardElement.loadImage("shipBot.png", scale);
    }

    @Override
    public Image getDestructionSprite(double scale) {

        return BoardElement.loadImage("shipExplosion.png", scale);
    }

}
