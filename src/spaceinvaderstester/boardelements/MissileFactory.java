package spaceinvaderstester.boardelements;

import javafx.animation.Animation;
import static javafx.animation.Animation.INDEFINITE;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.util.Duration;

/**
 *
 * @author Dean
 */
public class MissileFactory extends BoardElement {

    public MissileFactory(int positionX, int positionY) {
        super(3, 1, positionX, positionY);
        secondaryImage.setTranslateY(-scale.get());
    }

    @Override
    protected Animation getIdleAnimation() {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getIdleSprite(this.scale.get()), this.scale.get() * 3, 300, 300, true);
        return anim;
    }

    @Override
    protected Animation getDestructionAnimation(long duration) {
        SpriteAnimation anim = new SpriteAnimation(secondaryImage, getDestructionSprite(this.scale.get() * 3), this.scale.get() * 3, duration);
        return anim;
    }

    @Override
    public Image getMovingSprite(double scale) {
        if (getPositionY() <= 11) {
            return BoardElement.loadImage("contrTop.png", scale);
        } else {
            return BoardElement.loadImage("contrBot.png", scale);
        }
    }

    @Override
    public Image getIdleSprite(double scale) {
        if (getPositionY() <= 11) {
            return BoardElement.loadImage("contrTop.png", scale);
        } else {
            return BoardElement.loadImage("contrBot.png", scale);
        }
    }

    @Override
    public Image getCreationSprite(double scale) {
        if (getPositionY() <= 11) {
            return BoardElement.loadImage("contrTop.png", scale);
        } else {
            return BoardElement.loadImage("contrBot.png", scale);
        }
    }

    @Override
    public Image getDestructionSprite(double scale) {

        return BoardElement.loadImage("shipExplosion.png", scale);
    }

    @Override
    protected Animation getMovementAnimation(long duration) {
        SpriteAnimation anim = new SpriteAnimation(mainImage, getMovingSprite(this.scale.get()), this.scale.get() * 3, duration);
        return anim;
    }

}
