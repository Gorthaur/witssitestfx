package spaceinvaderstester;

import com.sun.javafx.tk.Toolkit;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import spaceinvaderstester.boardelements.Alien;
import spaceinvaderstester.boardelements.AlienFactory;
import spaceinvaderstester.boardelements.BoardElement;
import spaceinvaderstester.boardelements.Bullet;
import spaceinvaderstester.boardelements.Missile;
import spaceinvaderstester.boardelements.MissileFactory;
import spaceinvaderstester.boardelements.Shield;
import spaceinvaderstester.boardelements.Ship;
import za.ac.wits.witstestablespaceinvaders.SpaceInvaderObject;

/**
 *
 * @author Dean
 */
public class BoardPane extends Pane {

    GameState currentState;
    WaitableParallelTransition parallel;

    SimpleDoubleProperty scale = new SimpleDoubleProperty(25);

    public DoubleProperty scaleProperty() {
        return scale;
    }

    public BoardPane() {
        this.prefWidthProperty().bind(scaleProperty().multiply(17));
        this.prefHeightProperty().bind(scaleProperty().multiply(23));
        this.maxWidthProperty().bind(scaleProperty().multiply(17));
        this.maxHeightProperty().bind(scaleProperty().multiply(23));
        this.minWidthProperty().bind(scaleProperty().multiply(17));
        this.minHeightProperty().bind(scaleProperty().multiply(23));
        this.setClip(new Rectangle(0, 0, scaleProperty().get() * 17, scaleProperty().get() * 23));
        //this.setStyle("-fx-background-color:#fe8422");
        this.getStyleClass().add("board");
        this.getStylesheets().add(BoardPane.class.getResource("board.css").toExternalForm());
    }

    public void transitionState(GameState state, long animationTimeMillis) {
        if (Platform.isFxApplicationThread()) {
            if (parallel != null) {
                parallel.finish();
            }
            parallel = null;

            LinkedList<Animation> transitions = new LinkedList<>();
            if (currentState == null) {
                setState(state);
            } else if (currentState.round == state.round - 1) {
                transitionForward(state, transitions, animationTimeMillis);
            } else if (currentState.round == state.round + 1) {
                transitionBackward(state, animationTimeMillis);
            } else {
                setState(state);
            }
            parallel = new WaitableParallelTransition();

            parallel.addAll(transitions);
            parallel.p.setOnFinished((e)->{
                startIdle();
            });
            
            stopIdle();
            parallel.playFromStart();
        } else {
            Platform.runLater(() -> {
                transitionState(state, animationTimeMillis);
            });
        }

    }
    
    public void stopIdle() {
        for (Node n: this.getChildrenUnmodifiable()) {
            if (n instanceof BoardElement) {
                BoardElement b = (BoardElement)n;
                b.stopIdleAnimation();
            }
        }
    }
    
    public void startIdle() {
        for (Node n: this.getChildrenUnmodifiable()) {
            if (n instanceof BoardElement) {
                BoardElement b = (BoardElement)n;
                b.startIdleAnimation();
            }
        }
    }

    public void transitionBackward(GameState state, long animationTimeMillis) {
        setState(state);
    }

    public void transitionForward(GameState state, LinkedList<Animation> transitions, long animationTimeMillis) {
        LinkedList<Node> allElements = new LinkedList<>();
        allElements.addAll(this.getChildren());
        BoardElement currentBoard[][] = getBoardElements(allElements);
        for (Point2D alien : state.getAliens()) {
            int x = (int) alien.getX();
            int y = (int) alien.getY();
            LinkedList<Point2D> checkPoints = new LinkedList<>();
            checkPoints.add(new Point2D(1, 0));
            checkPoints.add(new Point2D(-1, 0));
            if (y <= 11) {
                checkPoints.add(new Point2D(0, 1)); //check behind the alien
            } else {
                checkPoints.add(new Point2D(0, -1));
            }
            addOrTransitionElement(checkPoints, alien, Alien.class, allElements, currentBoard, transitions, animationTimeMillis, state);
        }
        for (Point2D ship : state.getShips()) {
            LinkedList<Point2D> checkPoints = new LinkedList<>();
            checkPoints.add(new Point2D(1, 0));
            checkPoints.add(new Point2D(-1, 0));
            checkPoints.add(new Point2D(0, 0));
            addOrTransitionElement(checkPoints, ship, Ship.class, allElements, currentBoard, transitions, animationTimeMillis, state);
        }
        for (Point2D ship : state.getAlienFactories()) {
            LinkedList<Point2D> checkPoints = new LinkedList<>();
            checkPoints.add(new Point2D(0, 0));
            addOrTransitionElement(checkPoints, ship, AlienFactory.class, allElements, currentBoard, transitions, animationTimeMillis, state);
        }
        for (Point2D ship : state.getMissileFactories()) {
            LinkedList<Point2D> checkPoints = new LinkedList<>();
            checkPoints.add(new Point2D(0, 0));
            addOrTransitionElement(checkPoints, ship, MissileFactory.class, allElements, currentBoard, transitions, animationTimeMillis, state);
        }
        for (Point2D ship : state.getShields()) {
            LinkedList<Point2D> checkPoints = new LinkedList<>();
            checkPoints.add(new Point2D(0, 0));
            addOrTransitionElement(checkPoints, ship, Shield.class, allElements, currentBoard, transitions, animationTimeMillis, state);
        }
        for (Point2D ship : state.getMissilesDown()) {
            LinkedList<Point2D> checkPoints = new LinkedList<>();
            checkPoints.add(new Point2D(0, -1));
            addOrTransitionElement(checkPoints, ship, Missile.class, allElements, currentBoard, transitions, animationTimeMillis, state);
        }
        for (Point2D ship : state.getMissilesUp()) {
            LinkedList<Point2D> checkPoints = new LinkedList<>();
            checkPoints.add(new Point2D(0, 1));
            addOrTransitionElement(checkPoints, ship, Missile.class, allElements, currentBoard, transitions, animationTimeMillis, state);
        }
        for (Point2D ship : state.getBullets()) {
            LinkedList<Point2D> checkPoints = new LinkedList<>();
            if (ship.getY() <= 11) {
                checkPoints.add(new Point2D(0, 1));
            } else {
                checkPoints.add(new Point2D(0, -1));
            }
            addOrTransitionElement(checkPoints, ship, Bullet.class, allElements, currentBoard, transitions, animationTimeMillis, state);
        }
        destroyElements(state, allElements, transitions, animationTimeMillis);
        currentState = state;
    }

    public void destroyElements(GameState state, LinkedList<Node> allElements, LinkedList<Animation> transitions, long animationTimeMillis) {
        SpaceInvaderObject board[][] = currentState.getSpaceInvaderBoard();
        for (Node n : allElements) {
            if (n instanceof BoardElement) {
                BoardElement b = (BoardElement) n;
                if (b.getPositionX() < 0 || b.getPositionX() > 16 || b.getPositionY() < 0 || b.getPositionY() > 22) {
                    Animation destruction;
                    destruction = b.getDestructionAnimation(0, 0, animationTimeMillis);
                    destruction.setOnFinished((e) -> {
                        Platform.runLater(() -> this.getChildren().remove(b));
                    });
                    transitions.add(destruction);
                    continue; //get out of this case
                }
                int deltax = 0;
                int deltay = 0;
                if (board[b.getPositionX()][b.getPositionY()] == SpaceInvaderObject.PLAYER1_MISSILE) {
                    
                    deltay = -1;
                } else if (board[b.getPositionX()][b.getPositionY()] == SpaceInvaderObject.PLAYER2_MISSILE) {
                    deltay = 1;
                } else if (board[b.getPositionX()][b.getPositionY()] == SpaceInvaderObject.ALIEN_MISSILE) {
                    if (b.getPositionY() <= 11) {
                        deltay = -1;
                    } else {
                        deltay = 1;
                    }
                } else if (board[b.getPositionX()][b.getPositionY()] == SpaceInvaderObject.ALIEN) {
                    if (b.getPositionY() <= 11) { //player 2
                        if (state.player1Delta == -1) { //player 2's aliens going left
                            if (state.player2RightmostAlienX == 16) { //just went forward
                                deltay = -1;
                            } else {
                                deltax = -1;
                            }
                        } else {
                            if (state.player2LeftmostAlienX == 0) {
                                deltay = -1;
                            } else {
                                deltax = 1;
                            }
                        }
                    } else { //player 1
                        if (state.player2Delta == -1) { //player 1's aliens going left
                            if (state.player1RightmostAlienX == 16) {
                                deltay = 1;
                            } else {
                                deltax = -1;
                            }
                        } else {
                            if (state.player1LeftmostAlienX == 0) {
                                deltay = 1;
                            } else {
                                deltax = 1;
                            }
                        }
                    }
                }

                Animation destruction;

                destruction = b.getDestructionAnimation(deltax, deltay, animationTimeMillis);

                destruction.setOnFinished((e) -> {
                    this.getChildren().remove(b);
                });
                transitions.add(destruction);
            }
        }
    }

    public BoardElement[][] getBoardElements(LinkedList<Node> allElements) {
        BoardElement board[][] = new BoardElement[17][23];
        for (Node n : allElements) {
            if (n instanceof BoardElement) {
                BoardElement b = (BoardElement) n;
                if (b.getPositionX() >= 0 && b.getPositionX() <= 16 && b.getPositionY() >= 0 && b.getPositionY() <= 22) {
                    board[b.getPositionX()][b.getPositionY()] = b;
                }
            }
        }
        return board;
    }

    public void addOrTransitionElement(LinkedList<Point2D> checkPoints, Point2D element, Class<? extends BoardElement> cls, LinkedList<Node> allElements, BoardElement[][] currentBoard, LinkedList<Animation> transitions, long animationTimeMillis, GameState state) {
        int x = (int) element.getX();
        int y = (int) element.getY();
        BoardElement predecessor = getPredecessor(checkPoints, x, y, cls, currentBoard);
        if (predecessor != null) {
            allElements.remove(predecessor);
            currentBoard[predecessor.getPositionX()][predecessor.getPositionY()] = null;
            transitions.add(predecessor.getMovementAnimation(x, y, animationTimeMillis));

        } else {
            try {
                Constructor c = cls.getConstructor(int.class, int.class
                );
                BoardElement b;

                if (cls.getName()
                        .equals("spaceinvaderstester.boardelements.Alien")) {
                    c = cls.getConstructor(int.class, int.class, int.class);

                    if (y <= 11) {
                        int xdel = 0;
                        if (state.player1Delta == -1 && state.player2LeftmostAlienX == 0) {
                            xdel = 0;
                        } else if (state.player1Delta == -1) {
                            xdel = 1;
                        } else if (state.player1Delta == 1 && state.player2RightmostAlienX == 16) {
                            xdel = 0;
                        } else {
                            xdel = -1;
                        }
                        b = (Alien) (c.newInstance(x+xdel, 11, -1));
                        transitions.add(b.getCreationAnimation(x, y, animationTimeMillis));
                    } else {
                                            int xdel = 0;
                    if (state.player2Delta == -1 && state.player1LeftmostAlienX == 0) {
                        xdel = 0;
                    }
                    else if (state.player2Delta == -1) {
                        xdel = 1;
                    }
                    else if (state.player2Delta == 1 && state.player1RightmostAlienX == 16) {
                        xdel = 0;
                    }
                    else {
                        xdel = -1;
                    }
                        b = (Alien) (c.newInstance(x+xdel, 11, 1));
                        transitions.add(b.getCreationAnimation(x, y, animationTimeMillis));
                    }
                } else if (cls.getName()
                        .equals("spaceinvaderstester.boardelements.Bullet")) {

                    if (y <= 11) {
                        LinkedList<Point2D> alienCheckPoints = new LinkedList<>();
                        alienCheckPoints.add(new Point2D(1, 1));
                        alienCheckPoints.add(new Point2D(-1, 1));
                        alienCheckPoints.add(new Point2D(0, 1));
                        alienCheckPoints.add(new Point2D(0, 2));
                        Point2D firingAlien = getPredecessorPosition(alienCheckPoints, x, y, SpaceInvaderObject.ALIEN);
                        if (firingAlien != null) {
                            b = (BoardElement) (c.newInstance((int) firingAlien.getX(), (int) firingAlien.getY()));
                            transitions.add(b.getCreationAnimation(x, y, animationTimeMillis));
                        } else {
                            b = (BoardElement) (c.newInstance(x, y + 1));
                            transitions.add(b.getCreationAnimation(x, y, animationTimeMillis));
                        }
                    } else {
                        LinkedList<Point2D> alienCheckPoints = new LinkedList<>();
                        alienCheckPoints.add(new Point2D(1, -1));
                        alienCheckPoints.add(new Point2D(-1, -1));
                        alienCheckPoints.add(new Point2D(0, -2));
                        alienCheckPoints.add(new Point2D(0, -1));
                        Point2D firingAlien = getPredecessorPosition(alienCheckPoints, x, y, SpaceInvaderObject.ALIEN);
                        if (firingAlien != null) {
                            b = (BoardElement) (c.newInstance((int) firingAlien.getX(), (int) firingAlien.getY()));
                            transitions.add(b.getCreationAnimation(x, y, animationTimeMillis));
                        } else {
                            b = (BoardElement) (c.newInstance(x, y - 1));
                            transitions.add(b.getCreationAnimation(x, y, animationTimeMillis));
                        }
                    }

                } else if (cls.getName()
                        .equals("spaceinvaderstester.boardelements.Missile")) {
                    if (y <= 11) {
                        b = (BoardElement) (c.newInstance(x, y - 1));
                        transitions.add(b.getCreationAnimation(x, y, animationTimeMillis));
                    } else {
                        b = (BoardElement) (c.newInstance(x, y + 1));
                        transitions.add(b.getCreationAnimation(x, y, animationTimeMillis));
                    }
                } else {
                    b = (BoardElement) (c.newInstance(x, y));
                    transitions.add(b.getCreationAnimation(x, y, animationTimeMillis));
                }

                this.getChildren()
                        .add(b);
                //this.addBoardElement(b);
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(BoardPane.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public BoardElement getPredecessor(LinkedList<Point2D> checkPoints, int x, int y, Class cls, BoardElement[][] currentBoard) {
        for (Point2D m : checkPoints) {
            if (x + m.getX() < 0 || x + m.getX() > 16 || y + m.getY() < 0 || y + m.getY() > 22) {
                continue;
            }
            int newX = (int) (x + m.getX());
            int newY = (int) (y + m.getY());
            if (currentBoard[newX][newY] != null) {
                if (cls.isInstance(currentBoard[newX][newY])) {
                    BoardElement el = currentBoard[newX][newY];
                    return el;
                }
            }
        }
        return null;
    }

    public Point2D getPredecessorPosition(LinkedList<Point2D> checkPoints, int x, int y, SpaceInvaderObject type) {
        for (Point2D m : checkPoints) {
            if (x + m.getX() < 0 || x + m.getX() > 16 || y + m.getY() < 0 || y + m.getY() > 22) {
                continue;
            }
            int newX = (int) (x + m.getX());
            int newY = (int) (y + m.getY());
            if (currentState.getSpaceInvaderBoard()[newX][newY] == type) {
                return new Point2D(newX, newY);
            }
        }
        return null;
    }

    public void setState(GameState state) {
        if (parallel != null) {
            parallel.finish();
        }
        if (Platform.isFxApplicationThread()) {

            this.getChildren().clear();
            for (Point2D alien : state.getAliens()) {
                if (alien.getY() <= 11) {
                    addBoardElement(new Alien((int) alien.getX(), (int) alien.getY(), -1));
                } else {
                    addBoardElement(new Alien((int) alien.getX(), (int) alien.getY(), 1));
                }
            }

            for (Point2D player : state.getShips()) {
                addBoardElement(new Ship((int) player.getX(), (int) player.getY()));
            }

            for (Point2D bullet : state.getBullets()) {
                addBoardElement(new Bullet((int) bullet.getX(), (int) bullet.getY()));
            }

            for (Point2D missile : state.getMissilesUp()) {
                addBoardElement(new Missile((int) missile.getX(), (int) missile.getY()));
            }

            for (Point2D missile : state.getMissilesDown()) {
                addBoardElement(new Missile((int) missile.getX(), (int) missile.getY()));
            }

            for (Point2D factory : state.getMissileFactories()) {
                addBoardElement(new MissileFactory((int) factory.getX(), (int) factory.getY()));
            }

            for (Point2D factory : state.getAlienFactories()) {
                addBoardElement(new AlienFactory((int) factory.getX(), (int) factory.getY()));
            }
            for (Point2D shield : state.getShields()) {
                addBoardElement(new Shield((int) shield.getX(), (int) shield.getY()));
            }
            currentState = state;
        } else {
            Platform.runLater(() -> setState(state));
        }
    }

    public void clear() {
        Platform.runLater(() -> {
            this.getChildren().clear();
            this.currentState = null;
        });
    }

    public void addBoardElement(BoardElement b) {
        this.getChildren().add(b);
        b.startAnimation();
    }

    public void addAlien(Alien a) {
        this.getChildren().add(a);
        a.startAnimation();
    }

    public void addBullet(Bullet b) {
        this.getChildren().add(b);
        b.startAnimation();
    }

    public void addMissile(Missile m) {
        this.getChildren().add(m);
        m.startAnimation();
    }

    public Task getWaitingTask() {
        Task t = new Task() {

            @Override
            protected Object call() throws Exception {
                return null;
            }

        };
        return t;
    }
}
